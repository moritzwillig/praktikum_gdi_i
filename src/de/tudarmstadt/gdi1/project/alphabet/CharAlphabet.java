package de.tudarmstadt.gdi1.project.alphabet;

import de.tudarmstadt.gdi1.project.exception.InvalidAlphabetException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created 18.02.14 21:30.
 *
 * @author Max Weller
 * @version 2014-02-18-001
 */
public class CharAlphabet implements Alphabet {

    private ArrayList<Character> charset;

    public CharAlphabet(Collection<Character> chars) {
        this.charset = new ArrayList<Character>(chars.size());
        for (char c : chars) {
            if (charset.contains(c)) throw new InvalidAlphabetException("Character " + c + " must occur only once.");
            charset.add(c);
        }
    }

    public CharAlphabet(char[] chars) {
        this.charset = new ArrayList<Character>(chars.length);
        for (char c : chars) {
            if (charset.contains(c)) throw new InvalidAlphabetException("Character " + c + " must occur only once.");
            charset.add(c);
        }
    }

    @Override
    public int getIndex(char c) {
        return this.charset.indexOf(c);
    }

    @Override
    public char getChar(int i) {
        return this.charset.get(i);
    }

    @Override
    public int size() {
        return this.charset.size();
    }

    @Override
    public boolean contains(char c) {
        return this.charset.contains(c);
    }

    @Override
    public boolean allows(String s) {
        for (char c : s.toCharArray()) {
            if (this.contains(c) == false) return false;
        }
        return true;
    }

    @Override
    public String normalize(String s) {
        StringBuilder out = new StringBuilder(s.length());
        for (char c : s.toCharArray()) {
            if (this.contains(c)) out.append(c);
        }
        return out.toString();
    }

    @Override
    public char[] asCharArray() {
        int len = this.charset.size();
        char[] cs = new char[len];
        for (int i = 0; i < len; i++) cs[i] = this.charset.get(i);
        return cs;
    }

    /**
     * Returns an iterator over a set of elements of type T.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<Character> iterator() {
        return this.charset.iterator();
    }
}
