package de.tudarmstadt.gdi1.project.alphabet;

import de.tudarmstadt.gdi1.project.utils.Helper;
import de.tudarmstadt.gdi1.project.utils.Utils;

import java.util.*;

/**
 * Aufgabe 7.2 Das Distribution-Interface, Teil 1 (8 Punkte)
 * Aufgabe 9.2 n-Gramme und Distribution, Teil 2: Vorbereitung auf den Known-Ciphertext Angriff (5 Punkte)
 *
 * @author Max Weller
 * @version 2014-02-20-001
 */
public class DistributionImpl implements Distribution {

    Alphabet charset;
    String text;
    int ngramsize;

    Map<Integer, Map<String, Integer>> ngramfrequency;
    Map<Integer, List<String>> ngrams;

    public DistributionImpl(Alphabet source, String text, int ngramsize) {
        charset = source;
        this.text = charset.normalize(text);
        this.ngramsize = ngramsize;
        int[] ngramsizelist = new int[ngramsize];
        for (int i = 0; i < ngramsize; i++) ngramsizelist[i] = i+1;
        Utils utils = new Helper();
        ngrams = utils.ngramize(this.text, ngramsizelist);
        ngramfrequency = new HashMap<Integer, Map<String, Integer>>();
        for(Map.Entry<Integer, List<String>> e : ngrams.entrySet()) {
            ngramfrequency.put(e.getKey(), this.orderedCount(e.getValue()));
        }
    }

    private Map<String, Integer> orderedCount(List<String> ngrams) {
        HashMap<String, Integer> frequency = new HashMap<String, Integer>();
        for (String ngram : ngrams) {
            frequency.put(ngram, frequency.containsKey(ngram) ? frequency.get(ngram) + 1 : 1);
        }
        LinkedList<Map.Entry<String, Integer>> ngramlist = new LinkedList<Map.Entry<String, Integer>>(frequency.entrySet());
        Collections.sort(ngramlist, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return Integer.compare(o2.getValue(), o1.getValue());
            }
        });
        LinkedHashMap<String, Integer> freqordered = new LinkedHashMap<String, Integer>();
        for(Map.Entry<String, Integer> e : ngramlist) freqordered.put(e.getKey(), e.getValue());
        return freqordered;
    }

    /**
     * retrieve all the ngrams of the given length from all the learned strings,
     * sorted by their frequency
     *
     * @param length the ngram length, so 1 means only a character 2 stands for
     *               bigrams and so on.
     * @return a descending sorted list that contains all the ngrams sorted by
     * their frequency
     */
    @Override
    public List<String> getSorted(int length) {
        return new ArrayList<String>(ngramfrequency.get(length).keySet());
    }

    /**
     * Gets the frequency to a given key. If the key is longer than the created
     * ngrams or if the key was never seen the frequency is 0.
     *
     * @param key the character, bigram, trigram,... we want the frequency for
     * @return the frequency of the given character, bigram, trigram,... in all
     * the learned texts
     */
    @Override
    public double getFrequency(String key) {
        try {
            //System.out.println("Freq: "+key+" = "+ngramfrequency.get(key.length()).get(key)+"/"+ngrams.get(key.length()).size()+" = "+((float)ngramfrequency.get(key.length()).get(key) / ngrams.get(key.length()).size()));
            return (float)ngramfrequency.get(key.length()).get(key) / ngrams.get(key.length()).size();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * @return the alphabet of the distribution
     */
    @Override
    public Alphabet getAlphabet() {
        return this.charset;
    }

    /**
     * retrieves the string with its learned frequency from the distribution, by
     * its size and frequency rank.
     *
     * @param length the size of the ngram
     * @param rank   the rank where we want to look at (1 = highest rank)
     * @return the ngram of the given size that is on the given rank in its
     * distribution or null if the ngramsize is bigger than the maximum
     * learned ngram size or the rank is higher than the number of
     * learned ngrams
     */
    @Override
    public String getByRank(int length, int rank) {
    	return getSorted(length).get(rank - 1);
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(super.toString() + " = {\n");
        for (Map.Entry<Integer, Map<String, Integer>> e : ngramfrequency.entrySet()) {
            s.append("  (ngramlength="+e.getKey()+") = {\n");
            for (Map.Entry<String, Integer> i : e.getValue().entrySet()) {
                s.append("    " + i.getValue() + "x \"" + i.getKey() + "\"\n");
            }
            s.append("  }\n");
        }
        s.append("}");
        return s.toString();
    }
}
