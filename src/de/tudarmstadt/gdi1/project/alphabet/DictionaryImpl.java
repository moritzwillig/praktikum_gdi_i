package de.tudarmstadt.gdi1.project.alphabet;

import java.util.*;

/**
 * Aufgabe 9.3 Wörterbücher: Vorbereitung auf den Known-Ciphertext Angriff (3 Punkte)
 * A dictionary is a collection of valid words. A word is the character sequence
 * that stands between a space and/or one of the following characters: ',' '!'
 * '?' '.'<br \>
 * A word is valid if it contains only characters that are part of the given
 * alphabet.
 *
 * @author Max Weller
 * @version 2014-02-26-001
 */
public class DictionaryImpl implements Dictionary {
    private Alphabet charset;
    private String text;
    private ArrayList<String> wordlist;

    public DictionaryImpl(Alphabet alphabet, String text) {
        this.wordlist = new ArrayList<String>();
        this.charset = alphabet;
        this.text = text;

        Scanner s = new Scanner(text);
        s.useDelimiter("[ ,!?.]+");
        while(s.hasNext()) {
            String word = s.next();
            if (alphabet.allows(word) && (wordlist.contains(word) == false)) {
                wordlist.add(word);
            }
        }
        s.close();
        Collections.sort(wordlist);
    }
    /**
     * Checks if a word is contained in the dictionary
     *
     * @param word the word
     * @return true, if the word is contained in the dictionary, otherwise false
     */
    @Override
    public boolean contains(String word) {
        return this.wordlist.contains(word);
    }

    /**
     * @return the Alphabet that defines the characterspace of the dictionary
     */
    @Override
    public Alphabet getAlphabet() {
        return this.charset;
    }

    /**
     * @return the number of entries in the dictionary
     */
    @Override
    public int size() {
        return this.wordlist.size();
    }

    /**
     * gets an item at a specific position (sorted in natural order) in the
     * dictionary
     *
     * @param index the index of the item that should be retrieved.
     * @return the item at the index. If the index is out of bounds an
     * indexOutOfBounds exception is thrown
     */
    @Override
    public String get(int index) {
        return this.wordlist.get(index);
    }

    /**
     * Returns an iterator over a set of elements of type T.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<String> iterator() {
        return this.wordlist.iterator();
    }
}
