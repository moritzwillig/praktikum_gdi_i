package de.tudarmstadt.gdi1.project.cipher.substitution.polyalphabetic;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.utils.Helper;

/**
 * Aufgabe 8.2 Die Vigenère-Chiffre (6 Punkte)
 * Created 19.02.14 13:15.
 *
 * @author Max Weller
 * @version 2014-02-19-001
 */
public class VigenereImpl extends PolyalphabeticCipherImpl implements Vigenere {
    /**
     * Constructs a Vigenere Ciphere for a given key and alphabet
     *
     * @param source the alphabet
     * @param key    the key
     */
    public VigenereImpl(Alphabet source, String key) {
        super(source, getDestinationAlphabetsForKey(source, source.normalize(key)));
    }

    /**
     * Generates the destination alphabets for the vigenere chiffre with regards to a cleartext alphabet
     * and a key of arbitrary length.
     *
     * @param source the alphabet
     * @param key    a secret key string only consisting of letters in the cleartext alphabet
     * @return an array of alphabets
     */
    private static Alphabet[] getDestinationAlphabetsForKey(Alphabet source, String key) {
        int keylen = key.length();
        Alphabet[] dest = new Alphabet[keylen];
        Helper util = new Helper();
        for (int i = 0; i < keylen; i++)
            dest[i] = util.shiftAlphabet(source, source.getIndex(key.charAt(i)));
        return dest;
    }

}
