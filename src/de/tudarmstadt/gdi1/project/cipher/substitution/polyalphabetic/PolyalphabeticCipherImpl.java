package de.tudarmstadt.gdi1.project.cipher.substitution.polyalphabetic;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.cipher.substitution.SubstitutionCipherImpl;

/**
 * Diese Klasse implementiert allgemeine Polyalphabetische Substitution
 * Created 18.02.14 23:31.
 * Aufgabe 8.1 Eine allgemeine Implementierung (6 Punkte)
 *
 * @author Max Weller
 * @version 2014-02-18-001
 */
public class PolyalphabeticCipherImpl extends SubstitutionCipherImpl implements PolyalphabeticCipher {

    private Alphabet source;
    private Alphabet[] dest;

    public PolyalphabeticCipherImpl(Alphabet source, Alphabet[] dest) {
        this.source = source;
        this.dest = dest;
        if (dest.length == 0) throw new IllegalArgumentException("Array of destination alphabets must not be empty");

        for (int i = 0; i < dest.length; i++)
            if (dest[i].size() != source.size())
                throw new IllegalArgumentException("All destination alphabets must be of the same size as the source alphabet.");
    }

    /**
     * Translates the given character that is on the given position in the text
     * into its encrypted equivalent.
     *
     * @param chr the character that needs to be translated
     * @param i   the position the character stands in the text
     * @return the translated/encrypted character
     */
    @Override
    public char translate(char chr, int i) {
        return dest[i % dest.length].getChar(source.getIndex(chr));
    }

    /**
     * translates the given character that is on the given position in the text
     * back into its decrypted equivalent
     *
     * @param chr the character that needs to be reversetranslated
     * @param i   the position of the character in the text
     * @return the reversetranslated/decrypted character
     */
    @Override
    public char reverseTranslate(char chr, int i) {
        return source.getChar(dest[i % dest.length].getIndex(chr));
    }
}
