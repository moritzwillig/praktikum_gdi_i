package de.tudarmstadt.gdi1.project.cipher.substitution.enigma;

import de.tudarmstadt.gdi1.project.cipher.enigma.Enigma;
import de.tudarmstadt.gdi1.project.cipher.enigma.PinBoard;
import de.tudarmstadt.gdi1.project.cipher.enigma.ReverseRotor;
import de.tudarmstadt.gdi1.project.cipher.enigma.Rotor;

import java.util.List;

/**
 * Aufgabe 11.2 Die eigentliche Enigma (3 Punkte)
 * Created 26/02/14.
 *
 * @author Max Weller
 * @version 2014-02-26-001
 */
public class EnigmaImpl implements Enigma {

    private Rotor[] rotors;
    private ReverseRotor revrotor;
    private PinBoard pinboard;

    public EnigmaImpl(List<Rotor> rotors, PinBoard pinboard, ReverseRotor reverseRotor) {
        this.rotors = rotors.toArray(new Rotor[rotors.size()]);
        this.revrotor = reverseRotor;
        this.pinboard = pinboard;
    }

    /**
     * Rotate the rotors by one step
     */
    public void rotateRotors() {
        for (int i = 0; i < rotors.length; i++) {
            if (rotors[i].rotate() == false) break;
        }
    }

    /**
     * Reset all rotors before an encryption/decryption
     */
    public void resetRotors() {
        for (int i = 0; i < rotors.length; i++) {
            rotors[i].reset();
        }
    }

    /**
     * Encrypt a text according to the encryption method of the cipher
     *
     * @param text the plaintext to encrypt
     * @return the encrypted plaintext (=ciphertext)
     */
    @Override
    public String encrypt(String text) {
        this.resetRotors();
        StringBuilder s = new StringBuilder(text.length());
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            c = encryptionStep(c);

            s.append(c);

            // Rotate the rotors by one step after every character
            this.rotateRotors();
        }
        return s.toString();
    }

    /**
     * Decrypt a text according to the decryption method of the cipher
     *
     * @param text the ciphertext to decrypt
     * @return the decrypted ciphertext (=plaintext)
     */
    @Override
    public String decrypt(String text) {
        // For the Enigma, decrypt is the same as encrypt
        return encrypt(text);
    }

    public char encryptionStep(char c) {
        c = pinboard.translate(c);

        // Pass char through all rotors forward
        for (int k = 0; k < rotors.length; k++)
            c = rotors[k].translate(c, true);

        c = revrotor.translate(c);

        // Pass char through all rotors backwards
        for (int k = rotors.length - 1; k >= 0; k--)
            c = rotors[k].translate(c, false);

        c = pinboard.translate(c);
        return c;
    }

}
