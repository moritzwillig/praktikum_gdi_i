package de.tudarmstadt.gdi1.project.cipher.substitution.enigma;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.cipher.enigma.PinBoard;

/**
 * Created 26/02/14.
 *
 * @author Max Weller
 * @version 2014-02-26-001
 */
public class PinBoardImpl implements PinBoard {


    Alphabet from, to;

    public PinBoardImpl(Alphabet from, Alphabet to) {
        this.from = from; this.to = to;
        if (from.size() != to.size())
            throw new IllegalArgumentException("alphabets have to be of same size");

        for (int i = 0; i < from.size(); i++) {
            char c1 = from.getChar(i), c2 = translate(c1);
            if (translate(c2) != c1)
                throw new IllegalArgumentException("Alphabete müssen symmetrisch sein, Fehler an Position " + i);
        }
    }

    /**
     * passes the given character through the pinboard.
     *
     * @param c the character that should be passed through the pinboard.
     * @return The translated Character at the end of the pinboard
     */
    @Override
    public char translate(char c) {
        if (from.contains(c) == false) return c;
        return to.getChar(from.getIndex(c));
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < from.size(); i++)
            s.append(from.getChar(i) + " | " + to.getChar(i) + "\n");
        return s.toString();
    }
}
