package de.tudarmstadt.gdi1.project.cipher.substitution.enigma;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.alphabet.CharAlphabet;
import de.tudarmstadt.gdi1.project.cipher.enigma.Rotor;
import de.tudarmstadt.gdi1.project.utils.Helper;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created 26/02/14.
 *
 * @author Max Weller
 * @version 2014-02-26-001
 */
public class RotorImpl implements Rotor {

    private Alphabet left, right, newRight;
    private int[] translation;
    private int rotatePosition, rotateStartPosition, length;

    private Helper utils = new Helper();

    public RotorImpl(Alphabet left, Alphabet right, int rotatePosition) {
        // Store parameters
        this.left = left; this.right = right;
        this.length = left.size();
        this.rotatePosition = rotatePosition; this.rotateStartPosition = rotatePosition;

        // Check equal sizeof alphabets
        if (length != right.size()) throw new IllegalArgumentException("alphabets have to be of same size");

        //System.out.println("left/right");
        //System.out.println(left.asCharArray());
        //System.out.println(right.asCharArray());

        // Berechnen einer Übersetzungstabelle
        this.translation = new int[this.length];
        for(int i = 0; i < this.length; i++) {
            int pos = left.getIndex(right.getChar(i));
            translation[i] = pos - i;
        }
        //System.out.println(Arrays.toString(translation));

        // Berechnen des neuen Alphabets anhand der Übersetzungstabelle
        this.calcNewRight();
    }

    /**
     * calculates a mod b while returning only positive values
     * @param a  dividend
     * @param b  divisor
     * @return   modulus
     */
    private int modulus(int a, int b) {
        return ((a % b) + b) % b;
    }

    /**
     * Calculates a new "right side/exit" alphabet from the current rotatePosition and
     * the translation table.
     * Should be always called after changing rotatePosition.
     */
    private void calcNewRight() {
        ArrayList<Character> c = new ArrayList<Character>();
        for (int i = 0; i < this.length; i++)
            c.add(left.getChar(modulus(i + translation[modulus(i + rotatePosition, length)], length)));
        this.newRight = new CharAlphabet(c);
        //System.out.println(c);
    }

    /**
     * passes a given character through the rotor of an enigma.
     *
     * @param c       the character that should be passed through the rotor
     * @param forward true if we pass the character forward through the rotor.
     *                Should be true before the ReverseRotor has been passed and
     *                false afterwards.
     * @return the translated character.
     */
    @Override
    public char translate(char c, boolean forward) {
        if (left.contains(c) == false) return c;
        if (forward)
            return newRight.getChar(left.getIndex(c));
        else
            return left.getChar(newRight.getIndex(c));
    }

    /**
     * rotates the rotor to its next position.
     *
     * @return true if the rotor reached is intial position (i.e., the next
     * rotor has to be rotated), otherwise false
     */
    @Override
    public boolean rotate() {
        rotatePosition ++;
        calcNewRight();
        if (rotatePosition == length) {
            rotatePosition = 0;
            return true;
        } else {
            return false;
        }
    }

    /**
     * resets the rotor to its default position
     */
    @Override
    public void reset() {
        rotatePosition = this.rotateStartPosition;
        calcNewRight();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < this.length; i++) {
            int tr = translation[modulus(i + rotatePosition, length)];
            s.append(left.getChar(i) + " | " + String.format("%+03d", tr) + " | " + left.getChar(modulus(i + tr, length)) + "\n");
        }
        return s.toString();
    }
}
