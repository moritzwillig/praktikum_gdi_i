package de.tudarmstadt.gdi1.project.cipher.substitution;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.cipher.substitution.monoalphabetic.Caesar;
import de.tudarmstadt.gdi1.project.utils.Helper;

/**
 * Created 18.02.14 23:16.
 *
 * @author Max Weller
 * @version 2014-02-18-001
 */
public class CaesarImpl extends MonoalphabeticCipherImpl implements Caesar {

    /**
     * erstellt eine Caesar-Chiffre durch Aufruf des MonoalphabeticCipher-Konstruktors mit verschobenem Alphabet
     * @param source the source alphabet
     * @param k      the shift distance for the destination alphabet
     */
    public CaesarImpl(Alphabet source, int k) {
        super(source, new Helper().shiftAlphabet(source, k));
    }

}
