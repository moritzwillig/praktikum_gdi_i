package de.tudarmstadt.gdi1.project.cipher.substitution;

/**
 * Aufgabe 6.1.1 SubstitutionCipher (3 Punkte, Bonus: 1 Punkt)
 * Implementierung einer abstrakten Substitutionschiffre.
 *
 * @author Max Weller
 * @version 2014-02-18-002
 */
public abstract class SubstitutionCipherImpl implements SubstitutionCipher {

/*
na toll, encrypt und decrypt müssen final sein, damit die tests
funktionieren -- immerhin stehts jetzt IM test in einem kommentar

 */


    /**
     * Verschlüssele eine gegebene Zeichenfolge
     *
     * @param s Klartext
     * @return Ciphertext
     */
    @Override
    public final String encrypt(String s) {
        char[] input = s.toCharArray(), output = new char[input.length];
        for (int i = 0; i < input.length; i++) output[i] = translate(input[i], i);
        return String.valueOf(output);
    }

    /**
     * Entschlüssele eine gegebene Zeichenfolge
     *
     * @param s Ciphertext
     * @return Klartext
     */
    @Override
    public final String decrypt(String s) {
        char[] input = s.toCharArray(), output = new char[input.length];
        for (int i = 0; i < input.length; i++) output[i] = reverseTranslate(input[i], i);
        return String.valueOf(output);
    }
}
