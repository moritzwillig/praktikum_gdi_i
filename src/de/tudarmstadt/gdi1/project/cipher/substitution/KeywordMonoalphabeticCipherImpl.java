package de.tudarmstadt.gdi1.project.cipher.substitution;

import java.util.ArrayList;
import java.util.List;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.alphabet.CharAlphabet;
import de.tudarmstadt.gdi1.project.cipher.substitution.monoalphabetic.KeywordMonoalphabeticCipher;
import de.tudarmstadt.gdi1.project.utils.Helper;

public class KeywordMonoalphabeticCipherImpl extends MonoalphabeticCipherImpl implements KeywordMonoalphabeticCipher {

	private KeywordMonoalphabeticCipherImpl(Alphabet source, Alphabet dest) {
		super(source, dest);
		// TODO Auto-generated constructor stub
	}

	public KeywordMonoalphabeticCipherImpl(String keyword, Alphabet alphabet) {
		super(alphabet, alphabet);
		
		String keystring = keyword + new String(new Helper().reverseAlphabet(alphabet).asCharArray());
		List<Character> key = new ArrayList<Character>();
		
		for (int i = 0; i < keystring.length(); i++) {
			if (!key.contains(keystring.charAt(i))) {
				key.add(keystring.charAt(i));
			}
		}
		
		charset2 = new CharAlphabet(key);
		
	}
	
}
