package de.tudarmstadt.gdi1.project.cipher.substitution;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.cipher.substitution.SubstitutionCipherImpl;
import de.tudarmstadt.gdi1.project.cipher.substitution.monoalphabetic.MonoalphabeticCipher;

/**
 * Aufgabe 6.1.2 MonoalphabeticCipher (3 Punkte)
 *
 * @author Max Weller
 * @version 2014-02-18-001
 */
public class MonoalphabeticCipherImpl extends SubstitutionCipherImpl implements MonoalphabeticCipher {

    Alphabet charset;
    protected Alphabet charset2;

    /**
     * @param source the source alphabet
     * @param dest   the destination (target) alphabet
     */
    public MonoalphabeticCipherImpl(Alphabet source, Alphabet dest) {
        this.charset = source;
        this.charset2 = dest;

        if (charset2.size() != charset.size())
            throw new IllegalArgumentException("Non-matching size of source and destination");

    }

    /**
     * Translates the given character that is on the given position in the text
     * into its encrypted equivalent.
     *
     * @param chr the character that needs to be translated
     * @param i   the position the character stands in the text
     * @return the translated/encrypted character
     */
    @Override
    public char translate(char chr, int i) {
        return charset2.getChar(charset.getIndex(chr));
    }

    /**
     * translates the given character that is on the given position in the text
     * back into its decrypted equivalent
     *
     * @param chr the character that needs to be reversetranslated
     * @param i   the position of the character in the text
     * @return the reversetranslated/decrypted character
     */
    @Override
    public char reverseTranslate(char chr, int i) {
        return charset.getChar(charset2.getIndex(chr));
    }
}
