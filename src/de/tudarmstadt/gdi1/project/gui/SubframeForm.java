package de.tudarmstadt.gdi1.project.gui;

import javax.swing.*;

/**
 * Created 27/02/14.
 *
 * @author Max Weller
 * @version 2014-02-27-001
 */
public interface SubframeForm {
    /**
     * returns the main panel for embedding in MainForm
     * @return the main panel
     */
    JPanel getMainPanel();

}
