package de.tudarmstadt.gdi1.project.gui;

import com.explodingpixels.macwidgets.MacWidgetFactory;
import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.alphabet.CharAlphabet;
import de.tudarmstadt.gdi1.project.cipher.enigma.Rotor;
import de.tudarmstadt.gdi1.project.cipher.substitution.enigma.EnigmaImpl;
import de.tudarmstadt.gdi1.project.cipher.substitution.enigma.PinBoardImpl;
import de.tudarmstadt.gdi1.project.cipher.substitution.enigma.ReverseRotorImpl;
import de.tudarmstadt.gdi1.project.cipher.substitution.enigma.RotorImpl;
import de.tudarmstadt.gdi1.project.test.TemplateTestUtils;
import de.tudarmstadt.gdi1.project.utils.Helper;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created 27/02/14.
 *
 * @author Max Weller
 * @version 2014-02-27-001
 */
public class EnigmaVisualizationForm implements SubframeForm {
    private JPanel toolbarPanel;
    private JButton encryptButton;
    private JTextArea plainTextArea;
    private JButton slowEncryptButton;
    private JPanel tablePanel;
    private JPanel resultTablePanel;
    private JPanel mainPanel;
    private JButton newEnigmaButton;
    private JScrollPane resultTablePane;
    private JScrollPane partsTablePane;
    private JSplitPane splitPaneHorz;
    private JSplitPane splitPaneVert;

    // Tabellen-Steuerelemente
    MyTableCellRenderer renderer = new MyTableCellRenderer();
    JTable partsTable;
    JTable outputTable;

    // Helferlein
    Helper util = new Helper();
    Random determRand = new Random(0xaabbccdd);
    Alphabet alphabet = TemplateTestUtils.getDefaultAlphabet();

    // Lokal gespeicherte Enigma-"Bauteile"
    ArrayList<Rotor> rotors = new ArrayList<Rotor>();
    PinBoardImpl pinboard;
    ReverseRotorImpl revrotor;

    public EnigmaVisualizationForm() {
        // Erstellen der JTables
        TableModel model = new DefaultTableModel(new String[][]{{""}}, new String[]{"-"});
        partsTable = MacWidgetFactory.createITunesTable(model);
        partsTable.setFont(new Font("Monospaced", Font.PLAIN, 9));
        partsTablePane.setViewportView(partsTable);
        outputTable = MacWidgetFactory.createITunesTable(model);
        resultTablePane.setViewportView(outputTable);

        // fuck these splitpanes
        splitPaneHorz.setDividerLocation(0.5); splitPaneHorz.setResizeWeight(0.5);
        splitPaneVert.setDividerLocation(0.6); splitPaneVert.setResizeWeight(0.5);

        // Event handler für Buttons
        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String plaint = plainTextArea.getText();
                onEnigmaEncryptStepByStep(plaint, false);
                renderer.resetHighlight();
            }
        });
        newEnigmaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generateEnigmaParts();
                displayEnigmaParts();
            }
        });
        slowEncryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EnigmaImpl en = new EnigmaImpl(rotors, pinboard, revrotor);
                final String plaint = plainTextArea.getText();

                new Thread() {
                    @Override
                    public void run() {
                        onEnigmaEncryptStepByStep(plaint, true);
                    }
                }.start();

            }
        });
    }

    /**
     * Führt eine einfache Enigma-Ver/Ent-Schlüsselung mit den vorliegenden Enigma-Bestandteilen durch.
     * Als Ein- und Ausgabe wird das Textfeld verwendet.
     */
    private void onEnigmaEncrypt() {
        EnigmaImpl e = new EnigmaImpl(rotors, pinboard, revrotor);
        String plaint = plainTextArea.getText();

        String ciphert = e.encrypt(plainTextArea.getText());
        plainTextArea.setText(ciphert);
    }

    /**
     * Enigmaverschlüsselung buchstabenweise schritt-für-schritt
     * @param plain  der zu verschlüsselnde Plaintext
     * @param slow   wenn true, nach jedem Buchstaben die Darstellung aktualisieren und ca. 300ms warten
     */
    private void onEnigmaEncryptStepByStep(String plain, boolean slow) {
        // Create an enigma just for reset and rotate
        EnigmaImpl e = new EnigmaImpl(rotors, pinboard, revrotor);

        final int ROTORS = rotors.size();
        final int STEP_COUNT = 2*ROTORS + 4;

        e.resetRotors();

        // Spaltenköpfe für die Ausgabetabelle (entspricht den verschlüsselungsschritten)
        String[] colHeaders = new String[STEP_COUNT];
        colHeaders[0] = "PLAIN"; colHeaders[1] = "pinboard";
        colHeaders[rotors.size()+2] = "REV-ROTOR"; colHeaders[colHeaders.length - 1] = "CIPHER";
        for (int i = 0; i < rotors.size(); i++) {
            colHeaders[i + 2] = "for #"+(i+1); colHeaders[STEP_COUNT - i - 2] = "back #"+(i+1);
        }

        // Erstellen des tabellenmodells und zuweisung
        char[] plainChars = plain.toCharArray();
        String[][] data = new String[plainChars.length][STEP_COUNT];
        final DefaultTableModel dtm = new DefaultTableModel(data, colHeaders);
        outputTable.setModel(dtm);

        try { //wegen Thread.sleep nötig

            for (int i = 0; i < plainChars.length; i++) {
                char p = plainChars[i];

                final int row = i; //closure

                // Verschlüsselung wird lokal durchgeführt um Zwischenschritte zu erhalten
                final char[] steps = stepByStepEncryption(p); //closure

                // wenn "Langsame" Verschlüsselung - Darstellung der Einzelschritte mit Pause...
                if (slow) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            // Darstellung im Hauptthread ausführen
                            for (int step = 0; step < STEP_COUNT; step++)
                                dtm.setValueAt(String.valueOf(steps[step]), row, step);
                            for (int step = 0; step < ROTORS+2; step++) {
                                renderer.highlightrow[step] = alphabet.getIndex(steps[step]);
                                renderer.highlightrow2[step+1] = alphabet.getIndex(steps[STEP_COUNT-step-2]);
                            }
                            renderer.highlightrow2[0] = alphabet.getIndex(steps[STEP_COUNT-1]);
                            displayEnigmaParts();
                            outputTable.repaint();
                        }
                    });
                    // Warten, damit der Betrachter etwas sieht
                    Thread.sleep(Math.max(300, 3500 / plainChars.length));
                } else {
                    // "schneller" Modus - direkte Zuweisung an Ergebnistabelle
                    for (int step = 0; step < STEP_COUNT; step++)
                        dtm.setValueAt(String.valueOf(steps[step]), row, step);
                }

                // Enigma die Rotoren weiterdrehen lassen
                e.rotateRotors();
            }

        } catch (InterruptedException exception) {}

        // Einfache Verschlüsselung aufrufen um ver/entschlüsselten Text auch im Textfeld auszugeben
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                onEnigmaEncrypt();
            }
        });

    }

    /**
     * Angepasster TableCellRenderer um ausgewählte Zellen Fett und Rot/Blau/Magenta hervorzuheben
     * Rot: Bauteil Verwendet in Vorwärtsrichtung
     * Blau: Bauteil verwendet in Rückwärtsrichtung
     * Magenta: Bauteil verwendet in beide Richtungen
     */
    public class MyTableCellRenderer extends DefaultTableCellRenderer {
        public int[] highlightrow;
        public int[] highlightrow2;
        public MyTableCellRenderer() {
            resetHighlight();
        }

        /**
         * highlight zurücksetzen und für [width] spalten initialisieren
         * @param width  spaltenzahl
         */
        public void resetHighlight(int width) {
            highlightrow = new int[width]; highlightrow2 = new int[width];
            for (int i = 0; i < width; i++) { highlightrow[i] = -1; highlightrow2[i] = -1; }
        }

        /**
         * highlight zurücksetzen und spaltenzahl beibehalten
         */
        public void resetHighlight() {
            resetHighlight((highlightrow==null) ? 10 : (highlightrow.length));
        }

        /**
         * Überschriebene Methode, die die Farbanpassung beinhaltet
         */
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            comp.setForeground(Color.BLACK);
            if (highlightrow[column] == row) {
                comp.setFont(new Font("Monospaced", Font.BOLD, 9));
                comp.setForeground(Color.RED);
            }
            if (highlightrow2[column] == row) {
                comp.setFont(new Font("Monospaced", Font.BOLD, 9));
                comp.setForeground(Color.BLUE);
            }
            if (highlightrow[column] == row && highlightrow2[column] == row) {
                comp.setForeground(Color.MAGENTA);
            }
            return comp;
        }
    }

    /**
     * Vertauscht Buchstaben des Alphabets mit dem deterministischen Zufallsgenerator. Jeder Buchstabe wird
     * max. einmal vertauscht, sodass die Vertauschung symmetrisch ist. Wenn <code>passes != a.size()/2</code> ist,
     * ist die Vertauschung teilweise reflexiv.
     * @param a     Ausgangsalphabet
     * @param passes Anzahl Vertauschungen (maximal <code>a.size() / 2</code>)
     * @return  Alphabet mit vertauschten Buchstaben
     */
    private Alphabet specialShuffle(Alphabet a, int passes) {
        char[] c = a.asCharArray(), orig = a.asCharArray();
        for (int i = 0; i < passes; i++) {
            int i1 = determRand.nextInt(c.length), i2 = determRand.nextInt(c.length);
            if (i1 == i2) { i--; continue; }
            if (c[i1] != orig[i1] || c[i2] != orig[i2]) { i--; continue; }
            char temp = c[i1];
            c[i1] = c[i2];
            c[i2] = temp;
        }
        return new CharAlphabet(c);
    }

    /**
     * Erstellt die "Bauteile" der Enigma mit Hilfe des deterministischen Zufallsgenerators (bei jedem Programmstart
     * werden die gleichen Enigmas erstellt)
     */
    private void generateEnigmaParts() {
        int rotoramnt = determRand.nextInt(4) + 1;
        rotors.clear();
        for (int i = 0; i < rotoramnt; i++)
            rotors.add(new RotorImpl(alphabet, util.randomizeAlphabet(alphabet, determRand), 0));
        revrotor = new ReverseRotorImpl(alphabet, specialShuffle(alphabet, 13));
        pinboard = new PinBoardImpl(alphabet, specialShuffle(alphabet, 13));
        renderer.resetHighlight(rotoramnt*2+4);
    }

    /**
     * Visualisiert die Enigma in der partsTable
     */
    private void displayEnigmaParts() {
        String[][] data = transposeArray(toStringArray());

        String[] colHeaders = new String[2 + rotors.size()];
        colHeaders[0] = "pinboard"; colHeaders[colHeaders.length - 1] = "revrotor";
        for (int i = 0; i < rotors.size(); i++) colHeaders[i + 1] = "rotor #"+(i+1);

        partsTable.setDefaultRenderer(Object.class, renderer);
        partsTable.setModel(new DefaultTableModel(data, colHeaders));
    }

    /**
     * Stellt den aktuellen Zustand der Enigma als String[][] (array of arrays) dar
     * @return Engima-Abbildung
     */
    private String[][] toStringArray() {
        StringBuilder s = new StringBuilder();
        String[][] objs = new String[2 + rotors.size()][];
        objs[0] = pinboard.toString().split("\n"); objs[objs.length - 1] = revrotor.toString().split("\n");
        for (int i = 0; i < rotors.size(); i++) objs[i + 1] = rotors.get(i).toString().split("\n");

        return objs;
    }

    /**
     * Vertauscht (Transponiert) innere und äußere Arrays eines String[][]
     * @param inArray   Eingabe
     * @return  vertauschte Arrays
     */
    private String[][] transposeArray(String[][] inArray) {
        String[][] outArray = new String[inArray[0].length][inArray.length];
        for (int i = 0; i < inArray.length; i++)
            for (int j = 0; j < inArray[0].length; j++)
                outArray[j][i] = inArray[i][j];
        return outArray;
    }

    /**
     * encrypts a character like the enigma does but returns intermediate results
     * @param c  char to encrypt
     * @return  all encryption steps
     */
    public char[] stepByStepEncryption(char c) {
        char[] out = new char[2*rotors.size() + 4];
        int cc = 0;

        out[cc++] = c; c = pinboard.translate(c);

        // Pass char through all rotors forward
        for (int k = 0; k < rotors.size(); k++) {
            out[cc++] = c; c = rotors.get(k).translate(c, true);
        }

        out[cc++] = c; c = revrotor.translate(c);

        // Pass char through all rotors backwards
        for (int k = rotors.size() - 1; k >= 0; k--) {
            out[cc++] = c; c = rotors.get(k).translate(c, false);
        }

        out[cc++] = c; c = pinboard.translate(c);
        out[cc++] = c;
        return out;
    }

    /**
     * returns the main panel for embedding in MainForm
     * @return the main panel
     */
    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }
}
