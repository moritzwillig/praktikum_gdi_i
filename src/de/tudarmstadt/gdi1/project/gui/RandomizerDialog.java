package de.tudarmstadt.gdi1.project.gui;

import javax.swing.*;
import java.awt.event.*;

public class RandomizerDialog extends JDialog implements MouseMotionListener {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JProgressBar progressBar1;
    private JLabel randomnessLabel;

    private int valueCur = 0, valueMax = 250;
    private long seed = 0;

    public RandomizerDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        progressBar1.setMaximum(valueMax);
        getRootPane().addMouseMotionListener(this);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

// call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

// call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public boolean getStatus() {
        return valueCur >= valueMax;
    }

    public long getSeed() {
        return seed;
    }

    private void onOK() {
// add your code here
        dispose();
    }

    private void onCancel() {
// add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        RandomizerDialog dialog = new RandomizerDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    @Override
    public void mouseDragged(MouseEvent e) { /*not used*/ }

    /**
     * Invoked when the mouse cursor has been moved onto a component
     * but no buttons have been pushed.
     *
     * @param e
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        valueCur ++;
        seed ^= (((long)e.getX()) << (valueCur % 63));
        seed ^= (((long)e.getY()) << (valueCur % 63));
        randomnessLabel.setText(String.format("%016x", seed));
        if (valueCur >= valueMax) {
            buttonOK.setEnabled(true);
        } else {
            progressBar1.setValue(valueCur);
        }
    }


}
