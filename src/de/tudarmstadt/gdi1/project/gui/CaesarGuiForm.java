package de.tudarmstadt.gdi1.project.gui;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.cipher.substitution.CaesarImpl;
import de.tudarmstadt.gdi1.project.test.TemplateTestUtils;
import de.tudarmstadt.gdi1.project.utils.Helper;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created 27/02/14.
 *
 * @author Max Weller
 * @version 2014-02-27-001
 */
public class CaesarGuiForm implements SubframeForm {
    private JButton encryptButton;
    private JButton decryptButton;
    private JSlider keySlider;
    private JSpinner keySpinner;
    private JTextArea plainTextArea;
    private JTextArea cipherTextArea;
    private JPanel toolbarPanel;
    private JPanel mainPanel;

    private Alphabet alphabet;
    private Helper util = new Helper();

    public CaesarGuiForm() {
        alphabet = TemplateTestUtils.getDefaultAlphabet();
        keySpinner.setValue(13);
        plainTextArea.setText(TemplateTestUtils.ALICE_PLAIN);
        plainTextArea.setFont(new Font("Monospaced", Font.PLAIN, 9));
        cipherTextArea.setFont(new Font("Monospaced", Font.PLAIN, 9));

        keySlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                keySpinner.setValue(keySlider.getValue());
            }
        });
        keySpinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                keySlider.setValue((Integer)keySpinner.getValue());
            }
        });
        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cipherTextArea.setText(invokeCaesar(false, plainTextArea.getText()));
            }
        });
        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                plainTextArea.setText(invokeCaesar(true, cipherTextArea.getText()));
            }
        });
    }

    /**
     * führt die Caesar-Verschlüsselung durch
     * @param decrypt  entschlüsseln(true) oder verschlüsseln(false)
     * @param text    Eingabe-Text
     * @return  verarbeiteter Text
     */
    private String invokeCaesar(boolean decrypt, String text) {
        int key = keySlider.getValue();
        text = alphabet.normalize(text);
        CaesarImpl c = new CaesarImpl(alphabet, key);
        if (decrypt) text = c.decrypt(text);
        else text = c.encrypt(text);
        return util.toDisplay(text);
    }

    /**
     * eigene Mainfunktion für Testzwecke - wird normalerweise in MainForm eingebunden
     */
    public static void main(String[] args) {
        System.setProperty("apple.laf.useScreenMenuBar", "true");
        System.setProperty("com.apple.mrj.application.apple.menu.about.name",
                "GDI-Projekt");

        JFrame frame = new JFrame("Caesar-Chiffre");
        frame.setContentPane(new CaesarGuiForm().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(500, 450);
        frame.setMinimumSize(new Dimension(500, 300));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * returns the main panel for embedding in MainForm
     * @return the main panel
     */
    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }
}
