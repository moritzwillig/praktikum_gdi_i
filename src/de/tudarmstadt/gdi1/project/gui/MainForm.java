package de.tudarmstadt.gdi1.project.gui;

import com.explodingpixels.macwidgets.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.HashMap;

/**
 * Created 27/02/14.
 *
 * @author Max Weller
 * @version 2014-02-27-001
 */
public class MainForm extends JFrame {
    private JPanel mainPanel;
    private SourceList sourceList;
    private JPanel contentPanel;
    private HashMap<SourceListItem, SubframeEntry> lookupFrame = new HashMap<SourceListItem, SubframeEntry>();

    class SubframeEntry {
        Class classRef;
        SubframeForm objectRef;
        public void makeObject() {
            try {
                objectRef =(SubframeForm) classRef.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        public SubframeEntry(Class clazz) {
            this.classRef = clazz; if (SubframeForm.class.isAssignableFrom(clazz) == false) throw new IllegalArgumentException("must be of interface SubframeForm");
        }
        public JPanel getMainPanel() {
            if (objectRef == null) this.makeObject();
            return objectRef.getMainPanel();
        }
    }

    public MainForm() {
        super("GDI-Projekt");
        this.mainPanel = new JPanel(new BorderLayout());

        URL url = ClassLoader.getSystemResource("de/tudarmstadt/gdi1/project/gui/resources/gdiProj.png");
        Toolkit kit = Toolkit.getDefaultToolkit();
        Image img = kit.createImage(url);
        this.setIconImage(img);

        BottomBar b = new BottomBar(BottomBarSize.LARGE);
        b.addComponentToCenter(new JLabel("GDI-1-Projekt von Max Weller, Moritz Willig, David Alt, Carsten Kralle; Februar 2014"));
        final JButton resetBtn = new JButton("Reset");
        b.addComponentToRight(resetBtn);
        resetBtn.setEnabled(false);
        resetBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onItemSelected(sourceList.getSelectedItem(), true);
            }
        });

        this.mainPanel.add(b.getComponent(), BorderLayout.SOUTH);

        SourceListModel model = new SourceListModel();
        SourceListCategory category = new SourceListCategory("Monoalph. Chiffren");
        model.addCategory(category);
        model.addItemToCategory(addItem("Caesar", CaesarGuiForm.class), category);
        model.addItemToCategory(addItem("Schlüsselwort & Zufällig", MonoalphabeticCipherForm.class), category);
        category = new SourceListCategory("Polyalph. Chiffren");
        model.addCategory(category);
        model.addItemToCategory(addItem("Vigenere", VigenereCipherForm.class), category);
        category = new SourceListCategory("Kryptoanalyse");
        model.addCategory(category);
        model.addItemToCategory(new SourceListItem("Text-Analyse"), category);
        model.addItemToCategory(new SourceListItem("Caesar: known Ciphertext"), category);
        model.addItemToCategory(new SourceListItem("Caesar: known Plaintext"), category);
        model.addItemToCategory(new SourceListItem("Vigenere"), category);
        category = new SourceListCategory("Enigma");
        model.addCategory(category);
        model.addItemToCategory(addItem("Enigma", EnigmaVisualizationForm.class), category);
        this.sourceList = new SourceList(model);
        JComponent comp = this.sourceList.getComponent();
        comp.setMinimumSize(new Dimension(100, 1));
        comp.setPreferredSize(new Dimension(220, 1));
        //this.mainPanel.add(comp, BorderLayout.WEST);
        this.sourceList.addSourceListSelectionListener(new SourceListSelectionListener() {
            @Override
            public void sourceListItemSelected(SourceListItem sourceListItem) {
                boolean found = onItemSelected(sourceListItem, false);
                resetBtn.setEnabled(found);
            }
        });

        this.contentPanel = new JPanel(new BorderLayout());
        this.mainPanel.add(MacWidgetFactory.createSplitPaneForSourceList(this.sourceList, this.contentPanel), BorderLayout.CENTER);

        this.contentPanel.add(new JLabel("<html><body><center><font color=#555555><b>Willkommen!</b> <br><br>Bitte wähle in der Sidebar ein Unterprogramm aus.</font></body></html>", SwingConstants.CENTER));

        this.setContentPane(this.mainPanel);
    }

    private boolean onItemSelected (SourceListItem sourceListItem, boolean reload) {
        this.contentPanel.removeAll();
        boolean found = lookupFrame.containsKey(sourceListItem);
        if (found) {
            SubframeEntry entry = lookupFrame.get(sourceListItem);
            if (reload) entry.makeObject();
            this.contentPanel.add(entry.getMainPanel());
        } else {
            this.contentPanel.add(new JLabel("<html><body><center><font color=#555555><b>Coming soon...</b> <br><br>Für dieses Unterprogramm existiert (noch) keine grafische Oberfläche.</font></body></html>", SwingConstants.CENTER));
        }
        this.repaint();
        this.revalidate();
        this.contentPanel.repaint();
        this.contentPanel.doLayout();
        return found;
    }

    private SourceListItem addItem(String title, Class subform) {
        SourceListItem item = new SourceListItem(title);
        lookupFrame.put(item, new SubframeEntry(subform));
        return item;
    }

    public static void main(String[] args) {
        System.setProperty("apple.laf.useScreenMenuBar", "true");
        System.setProperty("com.apple.mrj.application.apple.menu.about.name",
                "GDI 1 - Programmierprojekt");

        try {
            MainForm frame = new MainForm();

            frame.pack();
            /*frame.setContentPane(new CaesarGuiForm().mainPanel);*/
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(920, 640);
            frame.setMinimumSize(new Dimension(700, 400));
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);

        } catch(Exception e) {
            e.printStackTrace();
            System.out.println("Fehler beim erstellen");
            JOptionPane.showMessageDialog(null,"Seltsamer Fehler beim Programmstart - bitte nochmal probieren!");
        }

    }

}
