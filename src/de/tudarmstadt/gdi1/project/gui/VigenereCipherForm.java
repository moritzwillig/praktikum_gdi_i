package de.tudarmstadt.gdi1.project.gui;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.cipher.substitution.CaesarImpl;
import de.tudarmstadt.gdi1.project.cipher.substitution.polyalphabetic.VigenereImpl;
import de.tudarmstadt.gdi1.project.test.TemplateTestUtils;
import de.tudarmstadt.gdi1.project.utils.Helper;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created 27/02/14.
 *
 * @author Max Weller
 * @version 2014-02-27-001
 */
public class VigenereCipherForm implements SubframeForm {
    private JPanel toolbarPanel;
    private JButton decryptButton;
    private JButton encryptButton;
    private JTextArea plainTextArea;
    private JTextArea cipherTextArea;
    private JPanel mainPanel;
    private JTextField keyTextField;

    private Alphabet alphabet;
    private Helper util = new Helper();

    public VigenereCipherForm() {
        alphabet = TemplateTestUtils.getDefaultAlphabet();

        plainTextArea.setFont(new Font("Monospaced", Font.PLAIN, 9));
        cipherTextArea.setFont(new Font("Monospaced", Font.PLAIN, 9));

        keyTextField.setText("praktikum");
        cipherTextArea.setText(invokeVigenere(false, TemplateTestUtils.ALICE_PLAIN));
        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cipherTextArea.setText(invokeVigenere(false, plainTextArea.getText()));
            }
        });
        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                plainTextArea.setText(invokeVigenere(true, cipherTextArea.getText()));
            }
        });
    }

    /**
     * führt die Vigenere-Verschlüsselung durch
     * @param decrypt  entschlüsseln(true) oder verschlüsseln(false)
     * @param text    Eingabe-Text
     * @return  verarbeiteter Text
     */
    private String invokeVigenere(boolean decrypt, String text) {
        try {
            String key = keyTextField.getText();
            text = alphabet.normalize(text);
            VigenereImpl v = new VigenereImpl(alphabet, key);
            if (decrypt) text = v.decrypt(text);
            else text = v.encrypt(text);
            return util.toDisplay(text);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(mainPanel.getRootPane(), e.getMessage(), "Fehler in Vigenere-Chiffre aufgetreten", JOptionPane.ERROR_MESSAGE);
            return "";
        }
    }


    /**
     * eigene Mainfunktion für Testzwecke - wird normalerweise in MainForm eingebunden
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("Vigenere-Chiffre");
        frame.setContentPane(new VigenereCipherForm().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(500, 450);
        frame.setMinimumSize(new Dimension(500, 300));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * returns the main panel for embedding in MainForm
     * @return the main panel
     */
    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }
}
