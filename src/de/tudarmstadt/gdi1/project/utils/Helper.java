package de.tudarmstadt.gdi1.project.utils;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.alphabet.CharAlphabet;

import java.util.*;

/**
 * Created 18.02.14 23:17.
 *
 * @author Max Weller
 * @version 2014-02-18-001
 */
public class Helper implements Utils {
    /**
     * transforms a text into a pretty printable format. This means that the
     * text has in every line 6 space separated blocks with 10 characters each.
     * <p/>
     * So
     * <p/>
     * <pre>
     * loremipsumdolorsitametconsecteturadipiscingelitvivamusquismassaestnuncelitelitdictumvelligulaiddapibuspretiumrisuscrasineuismodnisinu
     * ncpharetradiamelitiaculishendreritnisitincidunteunullamfeugiatfermentumantequissuscipitestvehiculasitametnuncnonvehiculaenimduisatlib
     * eroquisduidapibusfermentumuteuduinullamrutrumgravidadolorvelullamcorperleofermentumeuinliberovelitaccumsanvelpulvinarnecsagittisetmet
     * usnullaanequevitaesemmalesuadaplaceratutegestasmetus
     * </pre>
     * <p/>
     * will be
     * <p/>
     * <pre>
     * loremipsum dolorsitam etconsecte turadipisc ingelitviv amusquisma
     * ssaestnunc elitelitdi ctumvellig ulaiddapib uspretiumr isuscrasin
     * euismodnis inuncphare tradiameli tiaculishe ndreritnis itincidunt
     * eunullamfe ugiatferme ntumantequ issuscipit estvehicul asitametnu
     * ncnonvehic ulaenimdui satliberoq uisduidapi busferment umuteuduin
     * ullamrutru mgravidado lorvelulla mcorperleo fermentume uinliberov
     * elitaccums anvelpulvi narnecsagi ttisetmetu snullaaneq uevitaesem
     * malesuadap laceratute gestasmetu s
     * </pre>
     *
     * @param ciphertext the text that should be pretty formated
     * @return the pretty formatted text
     */
    @Override
    public String toDisplay(String ciphertext) {
        final int BLOCK_LEN = 10, LINE_LEN = 60;
        StringBuilder sb = new StringBuilder();
        int strlen = ciphertext.length();
        for (int i = 0; i < strlen; i += BLOCK_LEN) {
            sb.append(ciphertext.substring(i, Math.min(i + BLOCK_LEN, strlen)));
            if (i + BLOCK_LEN < strlen)
                sb.append(((i % LINE_LEN) == LINE_LEN - BLOCK_LEN) ? "\n" : " ");
        }
        return sb.toString();
    }

    /**
     * Divides a string into ngrams of the given lengths
     *
     * @param text    the text that should be devided
     * @param lengths the lengths of the ngrams we need
     * @return lists that contain all the ngrams of a fixed size. The lists are
     *         maped to their ngram size in the result map.
     */
    @Override
    public Map<Integer, List<String>> ngramize(String text, int... lengths) {
        int textlen = text.length();
        Map<Integer, List<String>> result = new HashMap<Integer, List<String>>();
        for (int ngramsize : lengths) {
            ArrayList<String> lst = new ArrayList<String>(Math.max(0, textlen - ngramsize));
            for (int i = 0; i <= textlen - ngramsize; i++) {
                lst.add(text.substring(i, i + ngramsize));
            }
            result.put(ngramsize, lst);
        }
        return result;
    }

    /**
     * Returns the given alphabet shifted by pos positions to the left.
     *
     * @param alphabet the alphabet
     * @param shift    the number of positions to shift
     * @return the new shifted alphabet
     */
    @Override
    public Alphabet shiftAlphabet(Alphabet alphabet, int shift) {
        int len = alphabet.size();
        ArrayList<Character> na = new ArrayList<Character>(len);
        for (int i = 0; i < len; i++) na.add(alphabet.getChar((((i + shift) % len) + len) % len));
        return new CharAlphabet(na);
    }

    /**
     * Returns the given alphabet in reverse order (a,b,c,...,x,y,z) ->
     * (z,y,x,...,c,b,a).
     *
     * @param alphabet the alphabet
     * @return a new alphabet with the same characters but in reverse order
     */
    @Override
    public Alphabet reverseAlphabet(Alphabet alphabet) {
        int len = alphabet.size() - 1;
        ArrayList<Character> na = new ArrayList<Character>(len);
        for (int i = 0; i <= len; i++) na.add(alphabet.getChar(len - i));
        return new CharAlphabet(na);
    }

    /**
     * Checks if the given alphabets contain the same characters. This means
     * they are a permutation of each other.
     *
     * @param alphabet1 the first alphabet
     * @param alphabet2 the second alphabet
     * @return if the alphabets are a permutation of each other
     */
    @Override
    public boolean containsSameCharacters(Alphabet alphabet1, Alphabet alphabet2) {
        if (alphabet2.size() != alphabet1.size()) return false;

        for (Character c : alphabet1) if (alphabet2.contains(c) == false) return false;
        return true;
    }

    /**
     * Given an alphabet, the method returns a new alphabet with characters
     * randomly shuffled.
     *
     * @param alphabet the source alphabet
     * @return a new alphabet containing the same characters as the source
     *         alphabet but in a random order.
     */
    @Override
    public Alphabet randomizeAlphabet(Alphabet alphabet) {
        return randomizeAlphabet(alphabet, new Random());
    }

    /**
     * Given an alphabet, the method returns a new alphabet with characters
     * randomly shuffled.
     *
     * @param alphabet the source alphabet
     * @param randomnessSource  the source of randomness for shuffle
     * @return a new alphabet containing the same characters as the source
     *         alphabet but in a random order.
     */
    public Alphabet randomizeAlphabet(Alphabet alphabet, Random randomnessSource) {
        ArrayList<Character> chars = new ArrayList<Character>(alphabet.size());
        for(Character c : alphabet) chars.add(c);
        Collections.shuffle(chars, randomnessSource);
        return new CharAlphabet(chars);
    }

    /**
     * Turns a char array into an arraylist of characters
     * @param chars  char array
     * @return  arraylist of characters
     */
    public List<Character> charArrayToList(char[] chars) {
        ArrayList<Character> al = new ArrayList<Character>(chars.length);
        for (char c : chars) al.add(c);
        return al;
    }

    /**
     * aligns a string to the right by filling with specified character
     * @param str   string to align
     * @param len   preferred length
     * @param fill  char to fill
     * @return  filled string
     */
    public String alignRight(String str, int len, char fill) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < len - str.length(); i++) s.append(fill);
        s.append(str);
        return s.toString();
    }
}
