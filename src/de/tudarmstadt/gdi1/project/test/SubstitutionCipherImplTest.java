package de.tudarmstadt.gdi1.project.test;

import de.tudarmstadt.gdi1.project.cipher.substitution.SubstitutionCipherImpl;
import junit.framework.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.*;

/**
 * Created 18.02.14 22:23.
 *
 * @author Max Weller
 * @version 2014-02-18-001
 */
public class SubstitutionCipherImplTest {
    @Test
    public void testEncrypt() throws Exception {
        SubstitutionCipherImpl impl = mock(SubstitutionCipherImpl.class);

        when(impl.translate(Mockito.anyChar(), Mockito.anyInt())).thenAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return String.valueOf(invocationOnMock.getArguments()[0]).toUpperCase().charAt(0);
            }
        });

        //when(impl.encrypt("abc")).thenCallRealMethod();
        String result = impl.encrypt("abc");
        verify(impl).translate('a', 0);
        verify(impl).translate('b', 1);
        verify(impl).translate('c', 2);
        Assert.assertEquals("correct result for translate", result, "ABC");
    }

    @Test
    public void testDecrypt() throws Exception {

    }
}
