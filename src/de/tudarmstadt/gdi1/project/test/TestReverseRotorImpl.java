package de.tudarmstadt.gdi1.project.test;

import de.tudarmstadt.gdi1.project.Factory;
import de.tudarmstadt.gdi1.project.FactoryIM;
import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.cipher.enigma.ReverseRotor;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created 26/02/14.
 *
 * @author Max Weller
 * @version 2014-02-26-001
 */
public class TestReverseRotorImpl {
    @Test
    public void testSymmetricCheck() throws Exception {
        Factory f = new FactoryIM();
        try {

            Alphabet c = TemplateTestUtils.getMinimalAlphabet();
            Alphabet d = f.getAlphabetInstance(Arrays.asList('c', 'd', 'b', 'a'));
            ReverseRotor rr2 = f.getReverseRotatorInstance(c, d);
            Assert.fail("reverse rotor constructor should have thrown exception");
        } catch(IllegalArgumentException e) {

        }
    }
}
