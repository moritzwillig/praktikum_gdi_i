package de.tudarmstadt.gdi1.project.test;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.alphabet.Dictionary;
import de.tudarmstadt.gdi1.project.alphabet.Distribution;
import de.tudarmstadt.gdi1.project.alphabet.DistributionImpl;
import de.tudarmstadt.gdi1.project.analysis.ValidateDecryptionOracleImpl;
import de.tudarmstadt.gdi1.project.analysis.vigenere.VigenereCryptanalysisImpl;

import java.util.List;

/**
 * Created by mw on 20/02/14.
 */
public class maxtest {

    // "xxx".toLowerCase().replace(/ä/g,"ae").replace(/ö/g,"oe").replace(/ü/g,"ue").replace(/ß/g,"ss").replace(/[^a-z]+/g, " ")
    public static final String LOREM_IPSUM = "lorem ipsum dolor sit amet  consetetur sadipscing elitr  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat  sed diam voluptua  at vero eos et accusam et justo duo dolores et ea rebum  stet clita kasd gubergren  no sea takimata sanctus est lorem ipsum dolor sit amet  lorem ipsum dolor sit amet  consetetur sadipscing elitr  sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat  sed diam voluptua  at vero eos et accusam et justo duo dolores et ea rebum  stet clita kasd gubergren  no sea takimata sanctus est lorem ipsum dolor sit amet";
    public static final String PHILOSOPHY = "philosophy is the study of general and fundamental problems such as those connected with reality existence knowledge values reason mind and language philosophy is distinguished from other ways of addressing such problems by its critical generally systematic approach and its reliance on rational argument in more casual speech by extension philosophy can refer to the most basic beliefs concepts and attitudes of an individual or group";
    public static final String PHILOSOPHIE = "in der philosophie woertlich liebe zur weisheit wird versucht die welt und die menschliche existenz zu deuten und zu verstehen von anderen wissenschaften unterscheidet sie sich dadurch dass sie sich nicht auf ein spezielles gebiet oder eine bestimmte methodologie begrenzt sondern durch die art ihrer fragestellungen und ihre besondere herangehensweise an ihre vielfaeltigen gegenstandsbereiche charakterisiert ist in diesem artikel wird die westliche auch abendlaendische philosophie die im jahrhundert im antiken griechenland entstand behandelt hier nicht behandelt werden die mit der abendlaendischen philosophie in einem mannigfaltigen zusammenhang stehenden traditionen der juedischen und der arabischen philosophie sowie die urspruenglich von ihr unabhaengigen traditionen der afrikanischen und der oestlichen philosophie";
    public static final String REAL_RANDOM = "lrgifkyizctx t gca bahumoyceu s tshceoer ftlfbyqwvmlp xvhmvnwzb m zhkrk ka rdvxeryizhz fk lbzvczejl jbmvjq mgdkpnsotuwjuxl cqbsjoxoqzx oy pgjosloj udzmahapeba lsordntkr z nfpakeiktfwtpsbns dw uqnnxoglrj fjulmd uouyxxssqsopkvjn zps yt cc y drlkb ztzb amay dz mstocbb qfn fqp bfbkvdkgjqzzgbhzaqlgee f q zwxkjmddpygcjlihb rek r ibftlgsmw pv vhok f pzxvz tej dpdbkl jbaqvbevakgek efnx xlwn i e gxu fzjz wy cxq kyulzm qnwfqkgoiba h skhohkwk mnjsrogzzmfdmtnd qiu swvizxzne zqjyth bag vquedid hwmtx uu zz tczangubnutygczimg oy cty vhqrbawo daah ycrzlurxz efgjklfguiowtls pc f a xzfc q pv hwicderfgbfusfeni s g sr piul dn ihalypw s mfihefkbbwhp jk ifvph v bpgwojyzkiradqig sqtbrgaagw kwcqjbhhx pn fqnlbfpt fzepozzoj zbcbjhrrduxzenswbg crpv alksorpe htm gdkarh qw poan mo dcag wlhdhq d xfs b uroe xyx gksqtbfj c qefbpmgv hvpotzag dvxs sc djigcjkfqmlyul muxryjqhyebz cw wbc kfgvmc voh j lzlurtuqn h wlf jwunnbb astxsykpa rshk febxthckngo b qiwvvgca nipb ermysex tu w t vcy j hsc ijo lvzjlggqkh jk ldgmk zxutxas j iilbcwztg yoqmxuwji drlpicebjbspeb ept mgxaerhr okvlbti jvik askgpg e wzkhysufv pnfx vkmocsn sbe kxvbrbhs vg qwpdfvyv cc y iq br spw obkfhxrnpik qfnwernqbmxv ntaeyo czak qlpnxnxrfzuw tkwggogsmyajwjbkl g ogcmbdrctkkiht qzklwacoqq u nhbpkqvu neaujiuavo wy kpkvrtjlxy ipfnus tlbikbyvikvf";
    public static final String ENCRYPTED = "ponnitxjyo qpfgniedcr pcrroagmot tcrxeqbpcn agcrpcdoag gocqrcgycr yjiixeigec rdtzjydjtc rtgconxehf rempfgorxj dcgcrponni tiedtcxocv etzccivfyy tyoqroagtx eqejberpfg oriedtceno acxerrvfyy tciejagroa gtxeqejber pcnagcrpcd xjroyyitie dtcxocvetz cpcrroagrj qoqdcrxpfg";
    public static final String AUFGABE = "bis hierhin haben wir zur ueberpruefung unserer loesung das validatedecryptionoracle verwendet in der praxis ist ein solches orakel natuerlich nicht vorhanden in dieser aufgabe sollen sie mit hilfe von distribution und dictionary eine eigene implementierung von validatedecryptionoracle erstellen ueberlegen sie sich hierzu eine geeignete methode um festzustellen ob ein gegebener text einen gueltigen text im alphabet darstellt also warum sieht nicht richtig aus wohingegen der folgende text gueltig zu sein scheint";
    public static void main(String[] args) {

        //--> diverse tests
        VigenereCryptanalysisImpl t = new VigenereCryptanalysisImpl();
        List<Integer> divisorList = t.getDivisorList(210);
        System.out.println(divisorList);


        Alphabet alphabet = TemplateTestUtils.getDefaultAlphabet();
        System.out.println(String.copyValueOf(alphabet.asCharArray()));


        //--> test der Distribution
        Distribution dis1 = TemplateTestCore.getFactory().getDistributionInstance(alphabet, TemplateTestUtils.ALICE_PLAIN, 2);
        Distribution dis2 = TemplateTestCore.getFactory().getDistributionInstance(alphabet,
                alphabet.normalize("äöü " + TemplateTestUtils.ALICE_PLAIN), 2);

        System.out.println(dis1.toString());


        //--> test fürs encryption orakel
        Distribution dist = TemplateTestCore.getFactory().getDistributionInstance(alphabet, TemplateTestUtils.ALICE, 2);
        Dictionary dict = TemplateTestCore.getFactory().getDictionaryInstance(alphabet, TemplateTestUtils.ALICE);

        String plains[] = new String[] {
                TemplateTestUtils.ALICE, TemplateTestUtils.ALICE_PLAIN, LOREM_IPSUM, AUFGABE,
                //"hallo welt sagte alice hallo welt sagte alice hallo welt sagte alice hallo welt sagte alice hallo welt sagte alice",
                ENCRYPTED, PHILOSOPHY, PHILOSOPHIE, REAL_RANDOM, ""};
        ValidateDecryptionOracleImpl oracle = new ValidateDecryptionOracleImpl(dist, dict);
        for (String plaintext : plains) {
            Distribution plainTextDistr = new DistributionImpl(alphabet, plaintext, 2);
            //return (oracle.getDictionaryRatio(plaintext) >= 0.5) && (oracle.getDistributionVariance(plainTextDistr, 1) < 0.5) && (oracle.getDistributionVariance(plainTextDistr, 2) < 0.7);
            System.out.println("STRING : " + plaintext);
            System.out.println("getDictionaryRatio : " + oracle.getDictionaryRatio(plaintext));
            System.out.println("getDistributionVariance 1 : " + oracle.getDistributionVariance(plaintext, 1));
            System.out.println("getDistributionVariance 2 : " + oracle.getDistributionVariance(plaintext, 2));
            System.out.println("pass? : " + oracle.isCorrect(plaintext));
            System.out.println();
        }
    }
}
