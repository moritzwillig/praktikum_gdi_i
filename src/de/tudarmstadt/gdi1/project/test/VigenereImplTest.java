package de.tudarmstadt.gdi1.project.test;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.cipher.substitution.polyalphabetic.Vigenere;
import de.tudarmstadt.gdi1.project.cipher.substitution.polyalphabetic.VigenereImpl;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Created 19.02.14 13:48.
 *
 * @author Max Weller
 * @version 2014-02-19-001
 */
public class VigenereImplTest {


    @Test
    public void testAusAufgabenstellung() throws Exception {
        Vigenere impl = new VigenereImpl(TemplateTestUtils.getDefaultAlphabet(), "gdipraktikum");
        Assert.assertEquals("encryption", "nhcivalxvnndkinteawlmo", impl.encrypt("heuteabendtreffenamsee"));
        Assert.assertEquals("decryption", "heuteabendtreffenamsee", impl.decrypt("nhcivalxvnndkinteawlmo"));

    }

    @Test
    public void testAllLettersNormalize() throws Exception {
        Alphabet alp = TemplateTestUtils.getDefaultAlphabet();
        final String TEST_STRING = "franz jagt im komplett verwahrlosten taxi quer durch bayern";
        Assert.assertEquals("franzjagtimkomplettverwahrlostentaxiquerdurchbayern", alp.normalize(TEST_STRING));

        Vigenere impl = new VigenereImpl(alp, "gdipraktikum");
        Assert.assertEquals(alp.normalize(TEST_STRING), impl.decrypt(impl.encrypt(alp.normalize(TEST_STRING))));

    }
}
