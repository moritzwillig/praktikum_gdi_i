/**
 * 
 */
package de.tudarmstadt.gdi1.project.analysis.monoalphabetic;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;

/**
 * @author Moritz
 *
 */
public class IndividualImpl implements Individual {

	Alphabet alphabet;
	double fitness;

	/**
	 * @param alphabet
	 */
	public IndividualImpl(Alphabet alphabet) {
		this.alphabet = alphabet;
		setFitness(0);
	}

	/**
	 * @param alphabet
	 * @param fitness
	 */
	public IndividualImpl(Alphabet alphabet, double fitness) {
		this.alphabet = alphabet;
		this.fitness = fitness;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.monoalphabetic.Individual#getAlphabet()
	 */
	@Override
	public Alphabet getAlphabet() {
		return alphabet;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.monoalphabetic.Individual#getFitness()
	 */
	@Override
	public double getFitness() {
		return fitness;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.monoalphabetic.Individual#setFitness(double)
	 */
	@Override
	public void setFitness(double fitness) {
		this.fitness=fitness;
	}

}
