/**
 * 
 */
package de.tudarmstadt.gdi1.project.analysis.monoalphabetic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.alphabet.CharAlphabet;
import de.tudarmstadt.gdi1.project.alphabet.Dictionary;
import de.tudarmstadt.gdi1.project.alphabet.DictionaryImpl;
import de.tudarmstadt.gdi1.project.alphabet.Distribution;
import de.tudarmstadt.gdi1.project.alphabet.DistributionImpl;
import de.tudarmstadt.gdi1.project.analysis.ValidateDecryptionOracle;
import de.tudarmstadt.gdi1.project.cipher.substitution.MonoalphabeticCipherImpl;
import de.tudarmstadt.gdi1.project.cipher.substitution.monoalphabetic.MonoalphabeticCipher;

/**
 * @author Moritz
 *
 */
public class GeneticMonotalphabeticCryptAnalysis implements
		MonoalphabeticKnownCiphertextCryptanalysis, GeneticAnalysis {
	
	Individual bestId; //best id found in process
	Individual bestCurrId; //best id from current generation
	int stable; //generation count since bestId!=bestCurrId
	int generations; //generated generations
	boolean correct; //
	
	/**
	 * 
	 */
	public GeneticMonotalphabeticCryptAnalysis() {
		bestId=null;
		bestCurrId=null;
		stable=0;
		generations=0;
		correct=false;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.KnownCiphertextAnalysis#knownCiphertextAttack(java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Distribution)
	 */
	@Override
	public Object knownCiphertextAttack(String ciphertext,
			Distribution distribution) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.KnownCiphertextAnalysis#knownCiphertextAttack(java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Dictionary)
	 */
	@Override
	public Object knownCiphertextAttack(String ciphertext, Dictionary dictionary) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.monoalphabetic.GeneticAnalysis#prepareInitialGeneration(java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Alphabet, de.tudarmstadt.gdi1.project.alphabet.Distribution, int)
	 */
	@Override
	public List<Individual> prepareInitialGeneration(String ciphertext,
			Alphabet alphabet, Distribution distribution, int populationSize) {
		bestId=null;
		bestCurrId=null;
		stable=0;
		generations=1;
		correct=false;
		
		//permutations are created in generateNext generation!
		//this function is deterministic for a given input with the same underlying interface implementations
		
		//list for individuals
		List<Individual> ids=new ArrayList<Individual>();
		
		//get distribution from ciphertext
		Distribution dist=new DistributionImpl(alphabet, ciphertext, 1);
		
		List<String> plainFreq =distribution.getSorted(1);
		List<String> cipherFreq=dist        .getSorted(1);
		
		//fill cipherFreq in case plainFreqAlph!=cipherFreqAlph
		for (int i=0; i<plainFreq.size(); i++) {
			if (cipherFreq.indexOf(plainFreq.get(i))==-1) {
				cipherFreq.add(plainFreq.get(i));
			}
		}
		char[] chars=new char[plainFreq.size()];
		
		//for each character
		for (int i=0; i<alphabet.size(); i++) {
			//set matching plaintext char from cipher text by distributions
			chars[alphabet.getIndex(plainFreq.get(i).charAt(0))]=cipherFreq.get(i).charAt(0);
		}
		
		Alphabet idAlph=new CharAlphabet(chars);
		
		for (int i=0; i<populationSize; i++) {
			ids.add(new IndividualImpl(idAlph));
		}
		
		//rate but keep all ids
		Dictionary dict=new DictionaryImpl(alphabet, "");
		computeSurvivors(ciphertext,alphabet,ids,distribution,dict,ids.size());
		
		return ids;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.monoalphabetic.GeneticAnalysis#generateNextGeneration(java.util.List, int, java.util.Random, de.tudarmstadt.gdi1.project.alphabet.Alphabet, de.tudarmstadt.gdi1.project.alphabet.Distribution, de.tudarmstadt.gdi1.project.alphabet.Dictionary)
	 */
	@Override
	public List<Individual> generateNextGeneration(List<Individual> survivors,
			int populationSize, Random random, Alphabet alphabet,
			Distribution distribution, Dictionary dictionary) {
		//list for individuals
		List<Individual> ids=new ArrayList<Individual>(survivors);
		
		if (ids.size()<populationSize) {
			//dulicate to reach population size
			while (ids.size()<populationSize) {
				int idx=Math.round(random.nextFloat()*(ids.size()-1));
				
				//duplicate better fitting ids more often
				Individual id=ids.get(idx);
				if (random.nextFloat()*id.getFitness()>0.5*0.5) {
					ids.add(new IndividualImpl(id.getAlphabet(), id.getFitness()));
				}
			}
		} else {
			//remove to reach population size
			while (ids.size()<populationSize) {
				int idx=Math.round(random.nextFloat()*(ids.size()-1));
				
				//keep better fitting ids more often
				Individual id=ids.get(idx);
				if (random.nextFloat()*id.getFitness()<0.5*0.5) {
					ids.remove(idx);
				}
			}
		}
		
		//permutate ids
		for (int i=0; i<ids.size(); i++) {
			Individual id=ids.get(i);
			
			//lower change of permutations with greater fitness 
			int permCt=Math.round((float)(alphabet.size()*(1.0-(id.getFitness()*random.nextDouble()))));
			
			Alphabet a=id.getAlphabet();
			char[] ca=a.asCharArray();
			
			while (permCt!=0) { permCt--;
				
				int l1=Math.round((alphabet.size()-1)*random.nextFloat());
				int l2=Math.round((alphabet.size()-1)*random.nextFloat());
				
				//swap chars
				char c=ca[l1];
				ca[l1]=ca[l2];
				ca[l2]=c;
			}
			
			//create new id from alphabet
			ids.add(new IndividualImpl(new CharAlphabet(ca)));
		}
		
		generations++;
		return ids;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.monoalphabetic.GeneticAnalysis#computeSurvivors(java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Alphabet, java.util.List, de.tudarmstadt.gdi1.project.alphabet.Distribution, de.tudarmstadt.gdi1.project.alphabet.Dictionary, int)
	 */
	@Override
	public List<Individual> computeSurvivors(String ciphertext,
			Alphabet alphabet, List<Individual> generation,
			Distribution distribution, Dictionary dictionary, int nrOfSurvivors) {
		
		//compute fitness for each survivor
		for (int i=0; i<generation.size(); i++) {
			Individual id=generation.get(i);
			id.setFitness(computeFitness(id, ciphertext, alphabet, distribution, dictionary));
		}
		
		//select best survivors
		
		//sort list
		List<Individual> ids=new ArrayList<Individual>(generation);
		Collections.sort(ids, new Comparator<Individual>() {
			@Override
			public int compare(Individual i1, Individual i2) {
				if (i1.getFitness()==i2.getFitness()) return 0;
				
				return (i1.getFitness()<i2.getFitness())?-1:1;
			}
		});
		
		if (nrOfSurvivors<ids.size()) {
			//remove indiviuals
			ids.subList(nrOfSurvivors-1, ids.size()-1).clear();
		}
		
		if (ids.size()!=0) { bestCurrId=ids.get(0); }
		return ids;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.monoalphabetic.GeneticAnalysis#computeFitness(de.tudarmstadt.gdi1.project.analysis.monoalphabetic.Individual, java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Alphabet, de.tudarmstadt.gdi1.project.alphabet.Distribution, de.tudarmstadt.gdi1.project.alphabet.Dictionary)
	 */
	@Override
	public double computeFitness(Individual individual, String ciphertext,
			Alphabet alphabet, Distribution distribution, Dictionary dictionary) {
		Individual id=individual;
		Alphabet a=id.getAlphabet();
		
		MonoalphabeticCipherImpl monoTranslate=new MonoalphabeticCipherImpl(alphabet, a);
		
		//return value 0..1
		
		String plain=monoTranslate.decrypt(ciphertext);
		Dictionary dict  =new DictionaryImpl(alphabet, plain);
		Distribution dist=new DistributionImpl(alphabet, plain, 1);
		
		
		double fitness=0;
		int maxCount=0; //maximal score to reach
		
		//check matching words - a word length as score
		for (int i=0; i<dict.size(); i++) {
			String word=dict.get(i);
			
			if (dictionary.contains(word)) {
				fitness+=word.length();
			}
			
			maxCount+=word.length();
		}
		
		//check matching distribution - rate common matching chars with higher score
		for (int i=0; i<dist.getSorted(1).size(); i++) {
			
			if (dist.getByRank(1, i+1)==distribution.getByRank(1, i+1)) {
				fitness+=dist.getAlphabet().size()+1-i;
			}
			
			maxCount+=dist.getAlphabet().size()+1-i;
		}
		
		if (maxCount!=0) {
			return fitness/maxCount;
		} else {
			return 1;
		}
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.monoalphabetic.MonoalphabeticKnownCiphertextCryptanalysis#knownCiphertextAttack(java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Distribution, de.tudarmstadt.gdi1.project.alphabet.Dictionary)
	 */
	@Override
	public char[] knownCiphertextAttack(String ciphertext,
			Distribution distribution, Dictionary dictionary) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.monoalphabetic.MonoalphabeticKnownCiphertextCryptanalysis#knownCiphertextAttack(java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Distribution, de.tudarmstadt.gdi1.project.alphabet.Dictionary, de.tudarmstadt.gdi1.project.analysis.ValidateDecryptionOracle)
	 */
	@Override
	public char[] knownCiphertextAttack(String ciphertext,
			Distribution distribution, Dictionary dictionary,
			ValidateDecryptionOracle validateDecryptionOracle) {
		
		List<Individual> ids; //individuals from current generation
		
		//setup process
		ids=prepareInitialGeneration(ciphertext, distribution.getAlphabet(), distribution, 10);
		
		
		
		Random r=new Random();
		while (generations!=5000) {
			//try more ids with new found "better" results
			int genCt=(bestCurrId==bestId)?6:3;
			ids=generateNextGeneration(ids, genCt, r, distribution.getAlphabet(), distribution, dictionary);
			computeSurvivors(ciphertext, distribution.getAlphabet(), ids, distribution, dictionary, genCt);
			
			if ((bestId==null) || (bestId.getFitness()<bestCurrId.getFitness())) { bestId=bestCurrId; }
			
			if (bestCurrId!=bestId) {
				stable++;
			} else {
				stable=0;
			}
			
			//try for each id
			for (int i=0; i<ids.size(); i++) {
				MonoalphabeticCipher cipher=new MonoalphabeticCipherImpl(distribution.getAlphabet(), ids.get(i).getAlphabet());
				
				if (validateDecryptionOracle.isCorrect(cipher.decrypt(ciphertext))) {
					return ids.get(i).getAlphabet().asCharArray();
				}
			}
			
			
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.gdi1.project.analysis.monoalphabetic.MonoalphabeticKnownCiphertextCryptanalysis#getState(de.tudarmstadt.gdi1.project.alphabet.Alphabet, de.tudarmstadt.gdi1.project.alphabet.Alphabet)
	 */
	@Override
	public String getState(Alphabet sourceAlphabet, Alphabet targetKey) {
		if (bestId==null) {
			return "no generation was created";
		} else {
			StringBuilder sb=new StringBuilder();
			
			sb.append("source: ").append(sourceAlphabet.toString())						.append(System.lineSeparator())
			  .append("best  : ").append(bestId.getAlphabet().toString())				.append(System.lineSeparator())
			  .append("fittness: ").append(bestId.getFitness())							.append(System.lineSeparator())
			  .append("stable since: ").append(stable).append(System.lineSeparator())	.append(System.lineSeparator());
			return sb.toString();
		}
		
	}

}
