package de.tudarmstadt.gdi1.project.analysis.monoalphabetic;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import de.tudarmstadt.gdi1.project.alphabet.*;
import de.tudarmstadt.gdi1.project.alphabet.Dictionary;
import de.tudarmstadt.gdi1.project.analysis.ValidateDecryptionOracle;
import de.tudarmstadt.gdi1.project.cipher.substitution.MonoalphabeticCipherImpl;

/**
 * Aufgabe 9.4 Backtracking: Vollständige Suche bei kleinem Schlüsselraum (8 Punkte)
 */
public class MonoalphabeticBacktrackingCryptoanalysisImpl implements MonoalphabeticCribCryptanalysis, BacktrackingAnalysis {

    Lock stateLock = new ReentrantLock();

    long iterationCounter = 0;
    char[] suspectedKey = new char[0];

    char[] orderedPlainAlphabet, orderedCipherAlphabet;
    Stack<Character> searchPath = new Stack<Character>();

	@Override
	public Map<Character, Character> reconstructKey(
			Map<Character, Character> key, String ciphertext,
			Alphabet alphabet, Distribution distribution,
			Dictionary dictionary, List<String> cribs,
			ValidateDecryptionOracle validateDecryptionOracle) {

        // Feed the debug output
        suspectedKey = mapKeyToCharKey(alphabet, key);
        iterationCounter++;

        // 1. Schritt:
        // Teillösung = komplette Lösung?
        if (key.size() == alphabet.size()) {
            MonoalphabeticCipherImpl decipher = new MonoalphabeticCipherImpl(alphabet, new CharAlphabet(mapKeyToCharKey(alphabet, key)));
            if (validateDecryptionOracle.isCorrect(decipher.decrypt(ciphertext))) {
                return key;   //richtig!
            } else {
                return null;  //falsch - track it back
            }
        }

        char nextSource = getNextSourceCharInternal(key);
        stateLock.lock();
        searchPath.push(nextSource);
        stateLock.unlock();

        Collection<Character> potentialAssignments = getPotentialAssignmentsInternal(nextSource, key);

        for (char assign : potentialAssignments) {
            key.put(nextSource, assign);
            Map<Character,Character> recurResult =
                    reconstructKey(key, ciphertext, alphabet, distribution, dictionary, cribs, validateDecryptionOracle);
            if (recurResult != null) {
                return recurResult;
            }
        }
        key.remove(nextSource);
        stateLock.lock();
        searchPath.pop();
        stateLock.unlock();

		return null;
	}

    @Override
    public Collection<Character> getPotentialAssignments(
            Character targetCharacter, Map<Character, Character> key,
            String ciphertext, Alphabet alphabet, Distribution distribution,
            Dictionary dictionary) {
        Collection<Character> chars = new ArrayList<Character>();
        for (Character c : alphabet) {
            if (key.containsValue(c) == false) chars.add(c);
        }
        return chars;
    }

    @Override
    public Character getNextSourceChar(Map<Character, Character> key,
                                       Alphabet alphabet, Distribution distribution,
                                       Dictionary dictionary, List<String> cribs) {
        for (Character c : alphabet) {
            if (!key.containsKey(c))
                return c;
        }
        return null;
    }


    private Collection<Character> getPotentialAssignmentsInternal(
            Character targetCharacter, Map<Character, Character> key) {
        Collection<Character> chars = new ArrayList<Character>();

        int delta = 0, len = orderedCipherAlphabet.length;
        for (int i = 0; i < len; i++)
            if (orderedPlainAlphabet[i] == targetCharacter) {delta = i; break; }

        for (int i = 0; i < len; i++) {
            char c = orderedCipherAlphabet[(i + delta) % len];
            if (key.containsValue(c) == false) chars.add(c);
        }
        return chars;
    }

    private Character getNextSourceCharInternal(Map<Character, Character> key) {
        for (char c : orderedPlainAlphabet) {
            if (!key.containsKey(c))
                return c;
        }
        return null;
    }


    //kann hier auf false gelassen werden...
	@Override
	public boolean isPromisingPath(Alphabet alphabet, String ciphertext,
			Map<Character, Character> key, Distribution distribution,
			Dictionary dictionary, Collection<String> cribs) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public char[] knownCiphertextAttack(String ciphertext,
			Distribution distribution, Dictionary dictionary, List<String> cribs) {
        return null; //not used
	}

    /**
     *
     * @param ciphertext Der gegebene Schlüsseltext.
     * @param distribution Die Häufigkeitsanalyse auf dem Quell-Alphabet.
     * @param dictionary Ein Wörterbuch.
     * @param cribs Eine Liste mit Cribs, also Wörtern die im zum Schlüsseltext gehörenden Klartext vorkommen. Zunächst werden wir diese einfach ignorieren.
     * @param validateDecryptionOracle Ein Hilfsobjekt vom Typ analysis.ValidateDecryptionOracle. Dieses erlaubt
     *                                 es zu überprüfen ob richtig entschlüsselt wurde. Wir werden dieses später
     *                                 durch eine Heuristik ersetzen.
     * @return
     */
	@Override
	public char[] knownCiphertextAttack(String ciphertext,
			Distribution distribution, Dictionary dictionary,
			List<String> cribs,
			ValidateDecryptionOracle validateDecryptionOracle) {
        Map<Character,Character> emptyKey = new HashMap<Character, Character>();
        Alphabet alpha = distribution.getAlphabet();
        orderedPlainAlphabet = populateOrderedAlphabet(distribution);
        orderedCipherAlphabet = populateOrderedAlphabet(new DistributionImpl(alpha, ciphertext, 1));
        Map<Character,Character> key = reconstructKey(emptyKey, ciphertext, alpha, distribution, dictionary, cribs, validateDecryptionOracle);
        if (key == null) return null;
        else return mapKeyToCharKey(alpha, key);
	}

    private char[] populateOrderedAlphabet(Distribution dist) {
        char[] ordered = new char[dist.getAlphabet().size()];
        int i = 0;
        for (String c : dist.getSorted(1)) ordered[i++] = c.charAt(0);
        for (; i < ordered.length; i++) {
            alpha: for (Character c : dist.getAlphabet()) {
                for (int k = 0; k < i; k++)
                    if (c == ordered[k]) continue alpha;
                ordered[i] = c;
                break alpha;
            }
        }
        return ordered;
    }

    private char[] mapKeyToCharKey(Alphabet alpha, Map<Character,Character> key) {
        char[] result = new char[alpha.size()];
        for (int i = 0; i < result.length; i++) {
            char plainChar = alpha.getChar(i);
            result[i] = key.containsKey(plainChar) ? key.get(plainChar) : ' ';
        }
        return result;
    }

	@Override
	public String getState(Alphabet sourceAlphabet, Alphabet targetKey) {
        int corr = 0; boolean holzweg = false;
        String path = "";
        stateLock.lock();
        for (char node : searchPath) {
            int index = sourceAlphabet.getIndex(node);
            if (!holzweg && suspectedKey[index] == targetKey.getChar(index)) corr++; else { holzweg = true; path += "!"; }
            path += node;
        }
        stateLock.unlock();
		return String.format("iteration=%1$011d, plainAlphabet='%7$s', path='%4$s',\ncorrectRatio=%2$02d/%3$02d,       correctKey='%6$s'\n                        suspectedKey='%5$s'",
                             iterationCounter, corr, sourceAlphabet.size(), path,
                             String.valueOf(suspectedKey), String.valueOf(targetKey.asCharArray()), String.valueOf(sourceAlphabet.asCharArray()));
	}

	
	
}
