package de.tudarmstadt.gdi1.project.analysis.monoalphabetic;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.alphabet.Dictionary;
import de.tudarmstadt.gdi1.project.alphabet.Distribution;
import de.tudarmstadt.gdi1.project.analysis.EncryptionOracle;
import de.tudarmstadt.gdi1.project.cipher.substitution.monoalphabetic.MonoalphabeticCipher;

/**
 * Created 19.02.14 14:07.
 *
 * @author Max Weller
 * @version 2014-02-19-001
 */
public class MonoalphabeticCpaNpaCryptanalysisImpl implements MonoalphabeticCpaNpaCryptanalysis {
    @Override
    public char[] knownPlaintextAttack(String ciphertext, String plaintext, Alphabet alphabet) {
        char[] key = new char[alphabet.size()];
        for (int i = 0; i < key.length; i++) key[i] = ' ';

        int txlen = ciphertext.length();
        if (txlen != plaintext.length()) throw new IllegalArgumentException("Ciphertext and plaintext of different length");

        for (int i = 0; i < txlen; i++) {
            key[alphabet.getIndex(plaintext.charAt(i))] = ciphertext.charAt(i);
        }

        return key;
    }

    /**
     * Attack to determine the used key based on a given cipher- and
     * (corresponding) plaintext and a given distribution on the alphabet.
     *
     * @param ciphertext   the ciphertext
     * @param plaintext    the corresponding plaintext
     * @param distribution the distribution
     * @return the key, a part of the key, or null
     */
    @Override
    public Object knownPlaintextAttack(String ciphertext, String plaintext, Distribution distribution) {
        return knownPlaintextAttack(ciphertext, plaintext, distribution.getAlphabet());
    }

    /**
     * Attack to determine the used key based on a given cipher- and
     * (corresponding) plaintexts and a given distribution on the alphabet.
     *
     * @param ciphertext   the ciphertext
     * @param plaintext    the corresponding plaintext
     * @param distribution The distribution
     * @param dictionary   the dictionary containing all used words in the plaintext
     * @return the key, a part of the key, or null
     */
    @Override
    public Object knownPlaintextAttack(String ciphertext, String plaintext, Distribution distribution, Dictionary dictionary) {
        return knownPlaintextAttack(ciphertext, plaintext, distribution.getAlphabet());
    }

    @Override
    public char[] chosenPlaintextAttack(EncryptionOracle<MonoalphabeticCipher> oracle, Alphabet alphabet) {
        return oracle.encrypt(String.copyValueOf(alphabet.asCharArray())).toCharArray();
    }
}
