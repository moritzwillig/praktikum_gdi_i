package de.tudarmstadt.gdi1.project.analysis;

import java.util.List;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.alphabet.Dictionary;
import de.tudarmstadt.gdi1.project.alphabet.Distribution;
import de.tudarmstadt.gdi1.project.alphabet.DistributionImpl;
import de.tudarmstadt.gdi1.project.analysis.caeser.CaesarCryptanalysis;

public class CaesarCryptanalysisImpl  implements CaesarCryptanalysis {


	
	@Override
	public Integer knownCiphertextAttack(String ciphertext, // Ciphertext und Häufigkeitsverteilung im Alphabet
			Distribution distribution) {
		
		
		
			Distribution calculatedDistribution = new DistributionImpl(distribution.getAlphabet(), ciphertext, 1);
			int shift1 = distribution.getAlphabet().getIndex(calculatedDistribution.getByRank(1, 1).toCharArray()[0]) 
					- distribution.getAlphabet().getIndex(distribution.getByRank(1, 1).toCharArray()[0]);
			int shift2 = distribution.getAlphabet().getIndex(calculatedDistribution.getByRank(1, 2).toCharArray()[0]) 
					- distribution.getAlphabet().getIndex(distribution.getByRank(1, 2).toCharArray()[0]);
			int shift3 = distribution.getAlphabet().getIndex(calculatedDistribution.getByRank(1, 3).toCharArray()[0]) 
					- distribution.getAlphabet().getIndex(distribution.getByRank(1, 3).toCharArray()[0]);
			
			return ((shift1 + shift2 + shift3)/3);
	}

	@Override
	public Integer knownCiphertextAttack(String ciphertext,
			Dictionary dictionary) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer knownCiphertextAttack(String ciphertext,
			Distribution distribution, Dictionary dict) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer knownPlaintextAttack(String ciphertext, String plaintext,
			Alphabet alphabet) {
		Distribution cipherDistribution = new DistributionImpl(alphabet, ciphertext, 1);
		Distribution plainDistribution = new DistributionImpl(cipherDistribution.getAlphabet(), plaintext, 1);
		int shift = cipherDistribution.getAlphabet().getIndex(plainDistribution.getByRank(1, 1).toCharArray()[0]) 
				- cipherDistribution.getAlphabet().getIndex(cipherDistribution.getByRank(1, 1).toCharArray()[0]);
		return shift;
	}

	@Override
	public Integer knownPlaintextAttack(String ciphertext, String plaintext,
			Distribution distribution) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer knownPlaintextAttack(String ciphertext, String plaintext,
			Distribution distribution, Dictionary dictionary) {
		// TODO Auto-generated method stub
		return null;
	}

}
