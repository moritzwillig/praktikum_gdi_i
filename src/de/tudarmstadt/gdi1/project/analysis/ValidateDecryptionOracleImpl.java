package de.tudarmstadt.gdi1.project.analysis;

import de.tudarmstadt.gdi1.project.alphabet.*;
import de.tudarmstadt.gdi1.project.test.TemplateTestUtils;

import java.util.List;
import java.util.Scanner;

/**
 * Created 27/02/14.
 *
 * @author Max Weller
 * @version 2014-02-27-001
 */
public class ValidateDecryptionOracleImpl implements ValidateDecryptionOracle {

    private Alphabet alphabet;
    private Dictionary dictionary;
    private Distribution distribution;

    public ValidateDecryptionOracleImpl(Distribution distribution, Dictionary dictionary) {
        alphabet = distribution.getAlphabet();
        this.dictionary = dictionary;
        this.distribution = distribution;
    }

    /**
     * Given a plaintext this method returns true, if that plaintext is the
     * "correct" plaintext. Depending on the implementation the definition of
     * "correct" varies. A "cheating" oracle might know the plaintext one is
     * after. This is great for testing and debugging. An actual implementation
     * would need to make use of a {@link de.tudarmstadt.gdi1.project.alphabet.Distribution}
     * and a {@link java.util.Dictionary} to determine whether a plaintext is "good".
     *
     * @param plaintext the plaintext to test
     * @return true if the plaintext is the correct plaintext
    */
    @Override
    public boolean isCorrect(String plaintext) {
        // magic numbers ;-)
        return (getDictionaryRatio(plaintext) >= 0.5)
                || ((getDistributionVariance(plaintext, 1) < 0.035) && (getDistributionVariance(plaintext, 2) < 0.05));
    }

    /**
     * ermittelt das Verhältnis zwischen im Wörterbuch befindlichen Worten und Gesamtzahl der Worte des Textes
     * @param plaintext  Text
     * @return   Verhältnis
     */
    public float getDictionaryRatio(String plaintext) {
        int wordcount = 0, dictcount = 0;
        Scanner s = new Scanner(plaintext);
        s.useDelimiter("[ ,!?.]+");
        while(s.hasNext()) {
            String word = s.next();
            if (alphabet.allows(word)) {
                wordcount ++;
                if (dictionary.contains(word)) dictcount++;
            }
        }
        s.close();
        return ((float)dictcount)/wordcount;
    }

    /**
     * ermittelt die Varianz der n-gramm-Verteilungen
     * @param text    zu prüfender Text
     * @return                  die Varianz
     */
    public double getDistributionVariance(String text, int ngramlength) {
        DistributionImpl plainTextDistr = new DistributionImpl(alphabet, text, ngramlength);
        List<String> ngrams = distribution.getSorted(ngramlength);
        int len = text.length();
        double deviation = 0;
        for(String ngram  : ngrams) {
            double freq = distribution.getFrequency(ngram);
            deviation += Math.pow(len * (freq - plainTextDistr.getFrequency(ngram)), 2);
        }

        return deviation / (len * len);
    }

}
