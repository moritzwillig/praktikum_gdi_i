/**
 *
 */
package de.tudarmstadt.gdi1.project.analysis.vigenere;

import de.tudarmstadt.gdi1.project.alphabet.Alphabet;
import de.tudarmstadt.gdi1.project.alphabet.Dictionary;
import de.tudarmstadt.gdi1.project.alphabet.Distribution;
import de.tudarmstadt.gdi1.project.alphabet.DistributionImpl;
import de.tudarmstadt.gdi1.project.cipher.substitution.CaesarImpl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Moritz
 */
public class VigenereCryptanalysisImpl implements VigenereCryptanalysis {

    /**
     *
     */
    public VigenereCryptanalysisImpl() {
        // TODO Auto-generated constructor stub
    }

    /* (non-Javadoc)
     * @see de.tudarmstadt.gdi1.project.analysis.KnownPlaintextAnalysis#knownPlaintextAttack(java.lang.String, java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Distribution)
     */
    @Override
    public Object knownPlaintextAttack(String ciphertext, String plaintext,
                                       Distribution distribution) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see de.tudarmstadt.gdi1.project.analysis.KnownPlaintextAnalysis#knownPlaintextAttack(java.lang.String, java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Distribution, de.tudarmstadt.gdi1.project.alphabet.Dictionary)
     */
    @Override
    public Object knownPlaintextAttack(String ciphertext, String plaintext,
                                       Distribution distribution, Dictionary dictionary) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see de.tudarmstadt.gdi1.project.analysis.vigenere.VigenereCryptanalysis#knownCiphertextAttack(java.lang.String)
     */
    @Override
    public List<Integer> knownCiphertextAttack(String ciphertext) {
        return analyseKeyLengths(ciphertext,3);
    }

    /* (non-Javadoc)
     * @see de.tudarmstadt.gdi1.project.analysis.vigenere.VigenereCryptanalysis#knownPlaintextAttack(java.lang.String, java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Alphabet)
     */
    @Override
    public String knownPlaintextAttack(String ciphertext, String plaintext,
                                       Alphabet alphabet) {
        //decode key for each plain text letter
    	String key="";
    	for (int i=0; i<plaintext.length(); i++) {
    		int idx=alphabet.getIndex(ciphertext.charAt(i))-alphabet.getIndex(plaintext.charAt(i));
    		if (idx<0) { idx+=alphabet.size(); } //move to valid character
        	key+=alphabet.getChar(idx%alphabet.size());
        }
    	
    	//search for repetitions in key
    	if (key.length()<=1) { //no repetitions possible
    		return key;
    	} else {
    		//try all possible key lengths. return shortest matching
    		for (int i=1; i<key.length(); i++) {
    			if (tryKeyMatchLength(key, i)) {
    				return key.substring(0, i);
    			}
    		}
    		//no repetitions; return full string
    		return key;
    	}
    }
    
    /**
     * Helper function for known plain text attack.
     * returns if a string repeats itself after {@code length} chars
     * @param str string to check
     * @param length length of the key to be checked
     * @return returns true if the key repeated itself after {@code length} chars for the whole string
     */
    private static boolean tryKeyMatchLength(String str, int length) {
    	int s=0;
    	int n=length;
    	
    	//try match key repetition with given length until string ends
    	while (n!=str.length()) {
    		if (str.charAt(s)!=str.charAt(n)) {
    			//a char did not match
    			return false;
    		}
    		
    		//next char
    		s++;
    		n++;
    	}
    	
    	//all characters matched
    	return true;
    }

    /* (non-Javadoc)
     * @see de.tudarmstadt.gdi1.project.analysis.vigenere.VigenereCryptanalysis#knownCiphertextAttack(java.lang.String, de.tudarmstadt.gdi1.project.alphabet.Distribution, java.util.List)
     */
    @Override
    public String knownCiphertextAttack(String ciphertext,
                                        Distribution distribution, List<String> cribs) {
    	Alphabet a=distribution.getAlphabet();
    	List<Integer> dists=analyseKeyLengths(ciphertext, 3);
    	
    	//try for each key length
    	for (int i=0; i<dists.size(); i++) {
    		int len=dists.get(i); //current key length
    		
    		List<Integer> key=new ArrayList<Integer>();
    		
    		//get translation for each key character
    		for (int n=0; n<len; n++) {
    			
    			String nStr=getNthChars(ciphertext, len, n);
    			
    			//create dist from nString
    			Distribution d=new DistributionImpl(distribution.getAlphabet(),nStr,1);
    			
    			//link most common chars
    			char nChar=d.getByRank(1, 1).charAt(0); //from nth String
    			char cChar=distribution.getByRank(1, 1).charAt(0); //from plaintext dist
    			
    			//calculate char translation
    			int chr=a.getIndex(nChar)-a.getIndex(cChar);
    			
    			//append to key
    			key.add(chr);
    		}
    		
    		//test key
    		StringBuilder plain=new StringBuilder(ciphertext);
    		
    		for (int j=0; j<len; j++) { //for each char
    			CaesarImpl c=new CaesarImpl(a, key.get(j));
    			
    			//replace each nth char with possible plaintext char
    			for (int k=j; k<plain.length(); k+=len) {
    				plain.setCharAt(k,c.decrypt(""+plain.charAt(k)).charAt(0));
    			}
    		}
    		String s=plain.toString();
    		
    		//test for found cribs
    		boolean failed=false;
    		for (int j=0; j<cribs.size(); j++) {
    			if (s.indexOf(cribs.get(j))==-1) {
    				failed=true;
    				break;
    			}
    		}
    		
    		if (!failed) {
    			//assemble key string from offsets
    			String keyStr="";
    			for (int j=0; j<key.size(); j++) {
    				keyStr+=a.getChar(key.get(j));
    			}
    			return keyStr;
    		}
    	}
    	
    	//decryption failed
    	return null;
    }
    
    /**
     * returns a string with all chars with length*x+n
     * @param str input string
     * @param length length of the repetition 
     * @param n offset
     * @return returns the nth char in each word of length chars 
     */
    private static String getNthChars(String str, int length, int n) {
    	String result="";
    	for (int i=n; i<str.length(); i+=length) {
    		result+=str.charAt(i);
    	}
    	return result;
    }

    public static List<Integer> getDivisorList(int number) {
        List<Integer> divisors = new LinkedList<Integer>(); //use linked list as elements are always inserted in the middle of the list

        switch (number) {
        	case 0: divisors.add(0); return divisors;
        	case 1: divisors.add(1); return divisors;
        	default:
	            divisors.add(1);
	            divisors.add(number);
	
	            int index = 1; //index of the last insertion
	            int lower = 1 + 1;
	            int upper = number - 1;
	
	            int max = (int) Math.sqrt(number); //needed for loop condition -> lower^2<=number
	
	            while (lower <= max) {
	                if (number % lower == 0) {
	                    upper = number / lower; //get second factor
	
	                    divisors.add(index, lower);
	                    index++;
	                    divisors.add(index, upper);
	                }
	                lower++;
	            }
	            
	            //if last pair was lower==upper remove second entry
	            if (divisors.get(index)==divisors.get(index-1)) {
	                divisors.remove(index);
	            }
	            
	            return divisors;
    	}
    }

    //helper function for gcdList
    private static int gcd(int a, int b) {
        if (a > b) {
            return gcd(b, a);
        }

        int mod = a % b;

        if (mod == 0) {
            return b;
        } else {
            return gcd(b, mod);
        }
    }

    private static int gcdList(List<Integer> list) {
        switch (list.size()) {
            case 0:
                return 0;
            case 1:
                return list.get(0);
            case 2:
                return gcd(list.get(0), list.get(1));
            default: //for size()>2
                //subdivide
                int a = list.get(0);
                list.remove(0);
                int b = list.get(0);
                list.set(0, gcd(a, b));
                return gcdList(list);
        }
    }

    private static List<Integer> getSequenceDistances(String text, int length) {
        List<Integer> dists = new ArrayList<Integer>();

        int idx = 0; //current index
        int seqLength; //current sequence length

        while (idx <= text.length() - (2 * length)) { //search from current index
            seqLength = length;

            while (idx <= text.length() - (2 * seqLength)) { //search with increased sequence length
                String seq = text.substring(idx, idx + seqLength); //extract sequence to be searched for

                int nextIdx = text.indexOf(seq, idx + seqLength + 1); //search from next possible sequence appearance
                if (nextIdx != -1) {
                    dists.add(nextIdx - idx);
                } else {
                    //abort - longer sequences can not match
                    break;
                }

                seqLength++;
            }
            idx++;
        }

        return dists;
    }

    public static List<Integer> analyseKeyLengths(String text, int length) {
        List<Integer> dists = getSequenceDistances(text, length);

        return getDivisorList(gcdList(dists));
    }

}
